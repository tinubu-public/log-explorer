parser grammar QueryParser;

options { tokenVocab = QueryLexer; }

query: simpleQuery <EOF>
       | uniformQuery <EOF>
       | nativeQuery <EOF>
       ;

simpleQuery: SIMPLE_QUERY queryText=S_TEXT? closeQuery=S_CLOSE_QUERY? ;
nativeQuery: NATIVE_QUERY queryText=N_TEXT? closeQuery=N_CLOSE_QUERY? ;
uniformQuery: UNIFORM_QUERY composedUniformQuery closeQuery=U_CLOSE_QUERY? ;

composedUniformQuery: queryIdentifier=U_IDENTIFIER? queryOperator=U_OPERATOR uniformQueryOperand # queryNode
                      | U_LPAR composedUniformQuery U_RPAR                                       # queryGroup
                      | U_NOT composedUniformQuery                                               # queryNegate
                      | composedUniformQuery U_AND composedUniformQuery                          # queryAnd
                      | composedUniformQuery U_OR composedUniformQuery                           # queryOr
                      ;
uniformQueryOperand: queryOperand=(U_NULL | U_INTEGER | U_DECIMAL | U_BOOLEAN | U_STRING | U_PATTERN) ;