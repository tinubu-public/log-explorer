/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.search;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Query date range.
 * {@link #fromDate()} must be set and is inclusive.
 * {@link #toDate()} must be set and is exclusive.
 *
 * @implSpec Immutable class implementation.
 */
public class QueryDateRange {
   private final ZonedDateTime fromDate;
   private final ZonedDateTime toDate;

   public QueryDateRange(ZonedDateTime fromDate, ZonedDateTime toDate) {
      this.fromDate = notNull(fromDate, "fromDate");
      this.toDate = notNull(toDate, "toDate");
   }

   /**
    * Truncates range dates to specified {@code unit}.
    *
    * @param unit unit to truncate range dates
    *
    * @return new range with truncated dates.
    */
   public QueryDateRange truncate(ChronoUnit unit) {
      return new QueryDateRange(fromDate.truncatedTo(unit), toDate.truncatedTo(unit));
   }

   public ZonedDateTime fromDate() {
      return fromDate;
   }

   public ZonedDateTime toDate() {
      return toDate;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      QueryDateRange that = (QueryDateRange) o;
      return Objects.equals(fromDate, that.fromDate) && Objects.equals(toDate, that.toDate);
   }

   @Override
   public int hashCode() {
      return Objects.hash(fromDate, toDate);
   }

   @Override
   public String toString() {
      return new ToStringBuilder(this).append("fromDate", fromDate).append("toDate", toDate).toString();
   }
}
