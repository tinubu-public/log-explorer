/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.search;

import static com.tinubu.commons.lang.util.ExceptionUtils.sneakyThrow;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.concurrent.atomic.AtomicReference;

import com.tinubu.logexplorer.core.backend.Backend;
import com.tinubu.logexplorer.core.export.Exporter;
import com.tinubu.logexplorer.core.parser.Parser;
import com.tinubu.logexplorer.core.progress.ProgressMeter;
import com.tinubu.logexplorer.core.search.query.QueryEngine;

/**
 * Support search execution logic.
 *
 * @apiNote Class life-cycle is global, not per-search.
 */
public abstract class AbstractSearch implements Search {
   protected final Backend backend;
   protected final ProgressMeter progressMeter;
   protected final Exporter exporter;
   protected final Parser parser;
   protected final QueryEngine uniformQueryEngine = new QueryEngine();

   protected AbstractSearch(Backend backend, ProgressMeter progressMeter, Parser parser, Exporter exporter) {
      this.backend = notNull(backend, "backendClient");
      this.progressMeter = notNull(progressMeter, "progressMeter");
      this.parser = notNull(parser, "parser");
      this.exporter = notNull(exporter, "exporter");
   }

   protected void searchError(Exception error) {
      throw new IllegalStateException(String.format("Error in '%s' backend > %s",
                                                    backend.getClass().getSimpleName(),
                                                    error.getMessage()), error);
   }

   /**
    * Propagates pipeline error if it occurred in parallel pipeline.
    */
   protected void propagatePipelineError(AtomicReference<Throwable> pipelineError) {
      Throwable pipelineThrowable = pipelineError.get();
      if (pipelineThrowable != null) {
         sneakyThrow(pipelineThrowable);
      }
   }

   public void close() {}

}
