/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.stacktrace;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Optional.ofNullable;

import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * {@link StackTrace} sub-model representing a method call in the trace.
 */
public class TracedMethod {

   /** Traced class name. */
   private Type className;
   /** Traced method name. */
   private String methodName;
   /** Traced file name. */
   private String fileName;
   /** Optional traced line. */
   private Integer line;

   /**
    * Builds a stack trace method from their elements.
    *
    * @param className traced class name
    * @param methodName traced method name
    * @param fileName traced file name
    * @param line optional traced line
    */
   public TracedMethod(Type className, String methodName, String fileName, Integer line) {
      this.className = notNull(className, "className");
      this.methodName = notNull(methodName, "methodName");
      this.fileName = notNull(fileName, "fileName");
      this.line = line;
   }

   /**
    * Builds a stack trace method from their mandatory elements.
    *
    * @param className traced class name
    * @param methodName traced method name
    * @param fileName traced file name
    */
   public TracedMethod(Type className, String methodName, String fileName) {
      this(className, methodName, fileName, null);
   }

   public Type className() {
      return className;
   }

   public String methodName() {
      return methodName;
   }

   public String fileName() {
      return fileName;
   }

   public Optional<Integer> line() {
      return ofNullable(line);
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      TracedMethod that = (TracedMethod) o;
      return Objects.equals(line, that.line) && Objects.equals(className, that.className) && Objects.equals(
            methodName,
            that.methodName) && Objects.equals(fileName, that.fileName);
   }

   @Override
   public int hashCode() {
      return Objects.hash(className, methodName, fileName, line);
   }

   @Override
   public String toString() {
      return new ToStringBuilder(this)
            .append("className", className)
            .append("methodName", methodName)
            .append("fileName", fileName)
            .append("line", line)
            .toString();
   }
}
