/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.stacktrace;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.regex.Pattern.DOTALL;
import static java.util.regex.Pattern.MULTILINE;
import static java.util.regex.Pattern.compile;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Parses a stacktrace represented as a {@link String}.
 */
class StackTraceParser {

   private final static Logger logger = LoggerFactory.getLogger(StackTraceParser.class);

   /**
    * @implNote Atomic group {@code (?>...)} is required at some place to workaround Pattern infinite
    *       back tracking loop and to simplify grouping.
    */
   protected static final Pattern ELEMENT_PATTERN = compile(
         "\\s*(?>(Wrapped by|Caused by):)?\\s*([^:]+):\\s*((?>.(?!(?>^\\s*at)))+)((?>.(?!^\\s*(?>Wrapped by:|Caused by:)))+)",
         MULTILINE | DOTALL);

   private static final Pattern TYPE_PATTERN =
         compile("(?>((?>[a-zA-Z_][a-zA-Z0-9_]*)(?>\\.(?>[a-zA-Z_][a-zA-Z0-9_]*))*)\\.)?([^.]+)");

   private static final Pattern TRACED_METHOD_PATTERN =
         compile("^\\s*at\\s*([^(]+)\\.([^(]+)\\(([^:)]+)(?>:([0-9]+))?\\)$", MULTILINE);

   /**
    * Parses the stack trace.
    *
    * @param stackTrace stack trace text to parse
    *
    * @return parsed stack trace
    *
    * @throws InvalidStackTraceException if stack trace has an invalid format
    */
   public StackTrace parse(String stackTrace) {
      RawStackTrace rawStackTrace = new RawStackTrace(notNull(stackTrace, "stackTrace"));

      if (rawStackTrace.analyzed()) {
         throw new InvalidStackTraceException("Empty stack trace");
      }

      StackTrace parsedStackTrace = new StackTrace();
      while (!rawStackTrace.analyzed()) {
         parseNextException(parsedStackTrace, rawStackTrace);
      }

      return parsedStackTrace;
   }

   private Type parseType(String type) {
      Type parsedType;
      Matcher matcher = TYPE_PATTERN.matcher(type.trim());
      if (matcher.matches()) {
         if (matcher.group(1) != null) {
            parsedType = new Type(matcher.group(1), matcher.group(2));
         } else {
            parsedType = new Type(matcher.group(2));
         }
      } else {
         throw new IllegalStateException(String.format("Can't parse type '%s'", type));
      }
      return parsedType;
   }

   /**
    * @throws InvalidStackTraceException if stack trace has an invalid format
    */
   private void parseNextException(StackTrace parsedStackTrace, RawStackTrace rawStackTrace) {
      Matcher matcher = ELEMENT_PATTERN.matcher(rawStackTrace.text());

      if (matcher.lookingAt()) {
         String elementType = matcher.group(1);
         if (elementType == null) {
            parsedStackTrace.pushRootException(new StackTraceException(parseType(matcher.group(2)),
                                                                       matcher.group(3).trim(),
                                                                       tracedMethods(matcher.group(4))));
         } else if ("Caused by".equals(elementType)) {
            parsedStackTrace.pushCauseException(new StackTraceException(parseType(matcher.group(2)),
                                                                        matcher.group(3).trim(),
                                                                        tracedMethods(matcher.group(4))));
         } else if ("Wrapped by".equals(elementType)) {
            parsedStackTrace.pushWrappingException(new StackTraceException(parseType(matcher.group(2)),
                                                                           matcher.group(3).trim(),
                                                                           tracedMethods(matcher.group(4))));
         } else {
            if (logger.isDebugEnabled()) {
               logger.debug("Unknown stack trace element '{}'", rawStackTrace.text());
            }
         }
         rawStackTrace.text(rawStackTrace.text().substring(matcher.end()));
      } else {
         throw new InvalidStackTraceException(String.format("Unknown stack trace format '%s'",
                                                            rawStackTrace.text()));
      }
   }

   private List<TracedMethod> tracedMethods(String rawTracedMethods) {
      List<TracedMethod> tracedMethods = new ArrayList<>();
      String parsedTracedMethods = rawTracedMethods;
      Matcher matcher = TRACED_METHOD_PATTERN.matcher(parsedTracedMethods);
      while (matcher.lookingAt()) {
         TracedMethod tracedMethod = new TracedMethod(parseType(matcher.group(1)),
                                                      matcher.group(2),
                                                      matcher.group(3),
                                                      matcher.group(4) != null
                                                      ? Integer.parseInt(matcher.group(4))
                                                      : null);
         tracedMethods.add(tracedMethod);
         parsedTracedMethods = parsedTracedMethods.substring(matcher.end());
         matcher = TRACED_METHOD_PATTERN.matcher(parsedTracedMethods);
      }
      return tracedMethods;
   }

   /**
    * Raw stack trace text to track parsing state.
    */
   private static class RawStackTrace {

      private String text;

      public RawStackTrace(String text) {
         this.text = notNull(text, "text");
      }

      public String text() {
         return text;
      }

      public void text(String text) {
         this.text = notNull(text, "text");
      }

      public boolean analyzed() {
         return text.trim().isEmpty();
      }

      @Override
      public String toString() {
         return new ToStringBuilder(this).append("text", text).toString();
      }
   }
}
