/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.config;

import static java.util.Collections.singletonList;

import java.net.URI;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * @implNote variables must be initialized with null values, so that configuration layers can be applied
 *       transparently without resetting unset variables on a given layer.
 */
public abstract class AbstractConfiguration implements Configuration {
   protected List<URI> backendUris;
   protected Integer connectTimeout;
   protected Integer socketTimeout;
   protected String authenticationToken;
   protected String authenticationUser;
   protected String authenticationPassword;
   protected AnsiFlag ansi;
   protected String parser;
   protected Formatter formatter;
   protected ProgressMeterFlag progress;
   protected QueryGroup query;
   protected QueryGroup notQuery;
   protected ZoneId timezone;
   protected String backend;
   protected Parameters parameters;
   protected Long numberLines;
   protected Aliases aliases;

   public AbstractConfiguration() {}

   public AbstractConfiguration(Configuration configuration) {
      this.backendUris = new ArrayList<>(configuration.backendUris());
      this.connectTimeout = configuration.connectTimeout();
      this.socketTimeout = configuration.socketTimeout();
      this.authenticationToken = configuration.authenticationToken();
      this.authenticationUser = configuration.authenticationUser();
      this.authenticationPassword = configuration.authenticationPassword();
      this.ansi = configuration.ansi();
      this.parser = configuration.parser();
      this.formatter = configuration.formatter();
      this.progress = configuration.progress();
      this.query = new QueryGroup(configuration.query());
      this.notQuery = new QueryGroup(configuration.notQuery());
      this.timezone = configuration.timezone();
      this.backend = configuration.backend();
      this.parameters = new Parameters(configuration.parameters());
      this.numberLines = configuration.numberLines();
      this.aliases = new Aliases(configuration.aliases());
   }

   public AbstractConfiguration withBackendUris(List<URI> backendUris) {
      this.backendUris = backendUris;
      return this;
   }

   public AbstractConfiguration withBackendUri(URI backendUri) {
      return withBackendUris(singletonList(backendUri));
   }

   public AbstractConfiguration withBackendUris(URI... backendUris) {
      return withBackendUris(Arrays.asList(backendUris));
   }

   public AbstractConfiguration withConnectTimeout(Integer connectTimeout) {
      this.connectTimeout = connectTimeout;
      return this;
   }

   public AbstractConfiguration withSocketTimeout(Integer socketTimeout) {
      this.socketTimeout = socketTimeout;
      return this;
   }

   public AbstractConfiguration withAuthenticationToken(String authenticationToken) {
      this.authenticationToken = authenticationToken;
      return this;
   }

   public AbstractConfiguration withAuthenticationUser(String authenticationUser) {
      this.authenticationUser = authenticationUser;
      return this;
   }

   public AbstractConfiguration withAuthenticationPassword(String authenticationPassword) {
      this.authenticationPassword = authenticationPassword;
      return this;
   }

   public AbstractConfiguration withAnsi(AnsiFlag ansi) {
      this.ansi = ansi;
      return this;
   }

   public AbstractConfiguration withParser(String parser) {
      this.parser = parser;
      return this;
   }

   public AbstractConfiguration withFormatter(Formatter formatter) {
      this.formatter = formatter;
      return this;
   }

   public AbstractConfiguration withProgress(ProgressMeterFlag progress) {
      this.progress = progress;
      return this;
   }

   public AbstractConfiguration withQuery(QueryGroup query) {
      if (this.query == null) {
         this.query = new QueryGroup();
      }
      this.query.merge(query);
      return this;
   }

   public AbstractConfiguration setQuery(QueryGroup query) {
      this.query = query;
      return this;
   }

   public AbstractConfiguration withNotQuery(QueryGroup notQuery) {
      if (this.notQuery == null) {
         this.notQuery = new QueryGroup();
      }
      this.notQuery.merge(notQuery);
      return this;
   }

   public AbstractConfiguration setNotQuery(QueryGroup notQuery) {
      this.notQuery = notQuery;
      return this;
   }

   public AbstractConfiguration withTimezone(ZoneId timezone) {
      this.timezone = timezone;
      return this;
   }

   public AbstractConfiguration withBackend(String backend) {
      this.backend = backend;
      return this;
   }

   public AbstractConfiguration withParameters(Parameters parameters) {
      if (this.parameters == null) {
         this.parameters = new Parameters();
      }
      this.parameters.merge(parameters);
      return this;
   }

   public AbstractConfiguration setParameters(Parameters parameters) {
      this.parameters = parameters;
      return this;
   }

   public AbstractConfiguration withNumberLines(Long numberLines) {
      this.numberLines = numberLines;
      return this;
   }

   public AbstractConfiguration withAliases(Aliases aliases) {
      if (this.aliases == null) {
         this.aliases = new Aliases();
      }
      this.aliases.merge(aliases);
      return this;
   }

   public List<URI> backendUris() {
      return backendUris;
   }

   public Integer connectTimeout() {
      return connectTimeout;
   }

   public Integer socketTimeout() {
      return socketTimeout;
   }

   public String authenticationToken() {
      return authenticationToken;
   }

   public String authenticationUser() { return authenticationUser; }

   public String authenticationPassword() { return authenticationPassword; }

   public AnsiFlag ansi() {
      return ansi;
   }

   public String parser() { return parser; }

   public Formatter formatter() { return formatter; }

   public ProgressMeterFlag progress() { return progress; }

   public QueryGroup query() { return query; }

   public QueryGroup notQuery() { return notQuery; }

   public ZoneId timezone() { return timezone; }

   public String backend() { return backend; }

   public Parameters parameters() { return parameters; }

   public Long numberLines() {
      return numberLines;
   }

   public Aliases aliases() {
      return aliases;
   }

   @Override
   public String toString() {
      return new ToStringBuilder(this)
            .append("backendUris", backendUris)
            .append("authenticationToken", authenticationToken)
            .append("authenticationUser", authenticationUser)
            .append("authenticationPassword", authenticationPassword)
            .append("connectTimeout", connectTimeout)
            .append("socketTimeout", socketTimeout)
            .append("ansi", ansi)
            .append("parser", parser)
            .append("formatter", formatter)
            .append("progress", progress)
            .append("query", query)
            .append("notQuery", notQuery)
            .append("timezone", timezone)
            .append("backend", backend)
            .append("parameters", parameters)
            .append("numberLines", numberLines)
            .append("aliases", aliases)
            .toString();
   }

}
