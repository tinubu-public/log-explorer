/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.formatter;

import com.tinubu.logexplorer.core.extension.Extension;
import com.tinubu.logexplorer.core.search.SearchResult.Attributes;

/**
 * Formatter interface for search result's {@link Attributes}.
 */
public interface Formatter extends Extension {

   /**
    * Format the specified attributes.
    *
    * @param stringBuilder string builder to append
    * @param attributes attributes to format
    */
   void format(StringBuilder stringBuilder, Attributes attributes);

   /**
    * Display string for this formatter in application.
    *
    * @return display string for this formatter
    */
   String displayString();
}
