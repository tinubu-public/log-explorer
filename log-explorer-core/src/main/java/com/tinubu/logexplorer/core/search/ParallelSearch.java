/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.search;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.time.temporal.ChronoUnit;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;

import org.apache.commons.lang3.time.StopWatch;

import com.tinubu.commons.lang.log.ExtendedLogger;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.logexplorer.core.backend.Backend;
import com.tinubu.logexplorer.core.export.Exporter;
import com.tinubu.logexplorer.core.parser.Parser;
import com.tinubu.logexplorer.core.progress.ProgressMeter;
import com.tinubu.logexplorer.core.search.query.QuerySpecification;

/**
 * Support search execution logic.
 *
 * @apiNote Class life-cycle is global, not per-search.
 */
public class ParallelSearch extends AbstractSearch {
   private static final ExtendedLogger logger = ExtendedLogger.of(ParallelSearch.class);

   public ParallelSearch(Backend backend, ProgressMeter progressMeter, Parser parser, Exporter exporter) {
      super(backend, progressMeter, parser, exporter);
   }

   @Override
   public void search(QuerySpecification querySpecification,
                      QueryDateRange dateRange,
                      Long numberLines,
                      boolean followMode) {
      notNull(querySpecification, "querySpecification");
      notNull(dateRange, "dateRange");

      dateRange = dateRange.truncate(ChronoUnit.MILLIS);

      AtomicInteger pipelineTaskCount = new AtomicInteger(0);
      AtomicReference<Throwable> pipelineError = new AtomicReference<>(null);

      SynchronousQueue<Pair<SearchResult, QuerySpecification>> resultQueue = new SynchronousQueue<>();
      Thread worker = workerThread(resultQueue, pipelineError, pipelineTaskCount);

      worker.start();

      try {
         StopWatch chrono = StopWatch.createStarted();

         backend.search(parallelPipeline(resultQueue, pipelineTaskCount, pipelineError),
                        querySpecification,
                        dateRange,
                        (numberLines == null || numberLines < 0) ? null : numberLines,
                        followMode);
         waitPipelineTasks(pipelineTaskCount);
         propagatePipelineError(pipelineError);

         logger.debug("Search duration = {} ms", () -> chrono.getTime(MILLISECONDS));
      } catch (Exception e) {
         searchError(e);
      } finally {
         progressMeter.close();
      }
   }

   private Thread workerThread(SynchronousQueue<Pair<SearchResult, QuerySpecification>> resultQueue,
                               AtomicReference<Throwable> pipelineError,
                               AtomicInteger pipelineTaskCount) {
      return new Thread(() -> {
         while (!Thread.currentThread().isInterrupted()) {
            try {
               Pair<SearchResult, QuerySpecification> nextResult = resultQueue.take();

               SearchResult result = parser.parse(nextResult.getLeft());
               result = uniformQueryEngine.filter(result, nextResult.getRight());
               exporter.export(result);
               progressMeter.handle(result);
            } catch (InterruptedException e) {
               Thread.currentThread().interrupt();
            } catch (Exception e) {
               pipelineError.set(e);
               throw e;
            } finally {
               pipelineTaskCount.decrementAndGet();
            }
         }
      });
   }

   /**
    * Waits for started tasks in parallel pipeline to finish.
    */
   private void waitPipelineTasks(AtomicInteger pipelineTaskCount) {
      while (pipelineTaskCount.get() > 0) {
         try {
            Thread.sleep(50);
         } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
         }
      }
   }

   /**
    * @implNote in case of error in parallel pipeline, an exception is throw in main thread to
    *       fail-fast processing. However, if error occurs in the very last result, pipeline error must also
    *       be managed on caller side.
    */
   private BiConsumer<SearchResult, QuerySpecification> parallelPipeline(SynchronousQueue<Pair<SearchResult, QuerySpecification>> queue,
                                                                         AtomicInteger pipelineTaskCount,
                                                                         AtomicReference<Throwable> pipelineError) {
      return (r, qs) -> {
         propagatePipelineError(pipelineError);

         pipelineTaskCount.incrementAndGet();
         try {
            queue.put(Pair.of(r, qs));
         } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
         }
      };
   }

}
