/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.search;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.time.temporal.ChronoUnit;
import java.util.function.BiConsumer;

import org.apache.commons.lang3.time.StopWatch;

import com.tinubu.commons.lang.log.ExtendedLogger;
import com.tinubu.logexplorer.core.backend.Backend;
import com.tinubu.logexplorer.core.export.Exporter;
import com.tinubu.logexplorer.core.parser.Parser;
import com.tinubu.logexplorer.core.progress.ProgressMeter;
import com.tinubu.logexplorer.core.search.query.QuerySpecification;

/**
 * Support search execution logic.
 *
 * @apiNote Class life-cycle is global, not per-search.
 */
public class SequentialSearch extends AbstractSearch {
   private static final ExtendedLogger logger = ExtendedLogger.of(SequentialSearch.class);

   public SequentialSearch(Backend backend, ProgressMeter progressMeter, Parser parser, Exporter exporter) {
      super(backend, progressMeter, parser, exporter);
   }

   @Override
   public void search(QuerySpecification querySpecification,
                      QueryDateRange dateRange,
                      Long numberLines,
                      boolean followMode) {
      notNull(querySpecification, "querySpecification");
      notNull(dateRange, "dateRange");

      dateRange = dateRange.truncate(ChronoUnit.MILLIS);

      try {
         StopWatch chrono = StopWatch.createStarted();

         backend.search(sequentialPipeline(),
                        querySpecification,
                        dateRange,
                        (numberLines == null || numberLines < 0) ? null : numberLines,
                        followMode);

         logger.debug("Search duration = {} ms", () -> chrono.getTime(MILLISECONDS));
      } catch (Exception e) {
         searchError(e);
      } finally {
         progressMeter.close();
      }
   }

   private BiConsumer<SearchResult, QuerySpecification> sequentialPipeline() {
      return (r, qs) -> {
         SearchResult result = parser.parse(r);
         result = uniformQueryEngine.filter(result, qs);
         progressMeter.handle(result);
         exporter.export(result);
      };
   }

}
