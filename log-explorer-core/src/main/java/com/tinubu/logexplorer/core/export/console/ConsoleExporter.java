/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.export.console;

import java.io.PrintStream;
import java.io.PrintWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.logexplorer.core.export.Exporter;
import com.tinubu.logexplorer.core.formatter.Formatter;
import com.tinubu.logexplorer.core.search.SearchResult;

public class ConsoleExporter implements Exporter {

   private static final Logger logger = LoggerFactory.getLogger(ConsoleExporter.class);

   private final PrintStream printStream;
   private final Formatter formatter;
   private final PrintWriter out;

   public ConsoleExporter(PrintStream printStream, Formatter formatter) {
      this.printStream = printStream;
      this.formatter = formatter;

      out = new PrintWriter(printStream);
      logger.debug("Export search results to console");
   }

   @Override
   public void close() {}

   public void export(SearchResult result) {
      StringBuilder resultBuffer = new StringBuilder();
      result.results().forEach(attrs -> {
         formatter.format(resultBuffer, attrs);
         resultBuffer.append(System.lineSeparator());
      });

      out.print(resultBuffer);
      out.flush();
   }

}
