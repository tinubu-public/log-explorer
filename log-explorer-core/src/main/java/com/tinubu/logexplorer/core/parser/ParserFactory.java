/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.parser;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.ServiceLoader;
import java.util.ServiceLoader.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.logexplorer.core.config.Configuration.Parameters;

public final class ParserFactory {

   private static final Logger logger = LoggerFactory.getLogger(ParserFactory.class);

   private static final ServiceLoader<ParserService> serviceLoader = serviceLoader();

   private ParserFactory() {
   }

   public static Parser instance(String parser, Parameters parameters) {
      notNull(parser, "parser");
      notNull(parameters, "parameters");

      Provider<ParserService> service = serviceLoader
            .stream()
            .filter(p -> p.get().name().equals(parser))
            .findFirst()
            .orElseThrow(() -> new IllegalStateException(String.format("No parser named '%s' has been found",
                                                                       parser)));

      return service.get().instance(parameters);
   }

   public static List<String> availableServices() {
      return ServiceLoader
            .load(ParserService.class)
            .stream()
            .map(service -> service.get().name())
            .collect(toList());
   }

   private static ServiceLoader<ParserService> serviceLoader() {
      final ServiceLoader<ParserService> serviceLoader = ServiceLoader.load(ParserService.class);

      if (logger.isDebugEnabled()) {
         serviceLoader
               .stream()
               .forEach(service -> logger.debug("Detected service : Parser '{}' ({})",
                                                service.get().name(),
                                                service.type().getName()));
      }
      return serviceLoader;
   }
}
