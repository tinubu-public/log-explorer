/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.stacktrace;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.junit.jupiter.api.Test;

class StackTraceParserTest {

   static {
      ToStringBuilder.setDefaultStyle(ToStringStyle.SHORT_PREFIX_STYLE);
   }

   @Test
   public void parseStackTraceWhenInvalidParameter() {
      assertThatThrownBy(() -> StackTrace.parse(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'stackTrace' must not be null");
   }

   @Test
   public void parseStackTraceWhenInvalidStackTrace() {
      assertThatThrownBy(() -> StackTrace.parse(""))
            .isInstanceOf(InvalidStackTraceException.class)
            .hasMessage("Empty stack trace");
      assertThatThrownBy(() -> StackTrace.parse(" "))
            .isInstanceOf(InvalidStackTraceException.class)
            .hasMessage("Empty stack trace");
      assertThatThrownBy(() -> StackTrace.parse("Message1"))
            .isInstanceOf(InvalidStackTraceException.class)
            .hasMessage("Unknown stack trace format 'Message1'");
   }

   @Test
   public void parseStackTraceWhenSimpleChain() {
      StackTrace stackTrace = StackTrace.parse(simpleStackTrace());
      assertThat(stackTrace).isNotNull();
      List<TracedMethod> tracedMethods =
            asList(new TracedMethod(new Type("o.a.c.d", "AtClass1"), "atMethod1", "AtClass1.java", 392),
                   new TracedMethod(new Type("org.pkg", "AtClass2"), "atMethod2", "AtClass2.java", 491));
      assertThat(stackTrace.exceptions()).containsExactly(new StackTraceException(new Type("java.pkg",
                                                                                           "RootException"),
                                                                                  "Root message line 1\n"
                                                                                  + "    root message line 2",
                                                                                  tracedMethods));
   }

   @Test
   public void parseStackTraceWhenUnknownElementChain() {
      StackTrace stackTrace = StackTrace.parse(unknownElementStackTrace());
      assertThat(stackTrace).isNotNull();
      List<TracedMethod> tracedMethods =
            asList(new TracedMethod(new Type("o.a.c.d", "AtClass1"), "atMethod1", "AtClass1.java", 392),
                   new TracedMethod(new Type("org.pkg", "AtClass2"), "atMethod2", "AtClass2.java", 491));
      assertThat(stackTrace.exceptions()).containsExactly(new StackTraceException(new Type("java.pkg",
                                                                                           "RootException"),
                                                                                  "Root message line 1\n"
                                                                                  + "    root message line 2",
                                                                                  tracedMethods));
   }

   @Test
   public void parseStackTraceWhenCausedByChain() {
      StackTrace stackTrace = StackTrace.parse(causedByStackTrace());
      assertThat(stackTrace).isNotNull();

      List<TracedMethod> tracedMethods =
            asList(new TracedMethod(new Type("o.a.c.d", "AtClass1"), "atMethod1", "AtClass1.java", 392),
                   new TracedMethod(new Type("org.pkg", "AtClass2"), "atMethod2", "AtClass2.java", 491));
      StackTraceException rootException = new StackTraceException(new Type("java.pkg", "RootException"),
                                                                  "Root message line 1\n"
                                                                  + "    root message line 2",
                                                                  tracedMethods);
      StackTraceException causeException1 = new StackTraceException(new Type("java.pkg", "CauseException1"),
                                                                    "Cause1 message line 1\n"
                                                                    + "    cause1 message line 2",
                                                                    tracedMethods);
      StackTraceException causeException2 = new StackTraceException(new Type("java.pkg", "CauseException2"),
                                                                    "Cause2 message line 1\n"
                                                                    + "    cause2 message line 2",
                                                                    tracedMethods);

      assertThat(stackTrace.exceptions()).containsExactly(rootException, causeException1, causeException2);

      assertThat(stackTrace.actualException()).isEqualTo(stackTrace.exceptions().get(0));
      checkCauseChain(stackTrace.exceptions().get(0),
                      stackTrace.exceptions().get(1),
                      stackTrace.exceptions().get(2));
      assertThat(stackTrace.rootCauseException()).isEqualTo(stackTrace.exceptions().get(2));
      checkWrappingChain(stackTrace.exceptions().get(2),
                         stackTrace.exceptions().get(1),
                         stackTrace.exceptions().get(0));
   }

   @Test
   public void parseStackTraceWhenWrappedByChain() {
      StackTrace stackTrace = StackTrace.parse(wrappedByStackTrace());
      assertThat(stackTrace).isNotNull();

      List<TracedMethod> tracedMethods =
            asList(new TracedMethod(new Type("o.a.c.d", "AtClass1"), "atMethod1", "AtClass1.java", 392),
                   new TracedMethod(new Type("org.pkg", "AtClass2"), "atMethod2", "AtClass2.java", 491));
      StackTraceException rootException = new StackTraceException(new Type("java.pkg", "RootException"),
                                                                  "Root message line 1\n"
                                                                  + "    root message line 2",
                                                                  tracedMethods);
      StackTraceException wrappingException1 =
            new StackTraceException(new Type("java.pkg", "WrappingException1"),
                                    "Wrapping1 message line 1\n" + "    wrapping1 message line 2",
                                    tracedMethods);
      StackTraceException wrappingException2 =
            new StackTraceException(new Type("java.pkg", "WrappingException2"),
                                    "Wrapping2 message line 1\n" + "    wrapping2 message line 2",
                                    tracedMethods);

      assertThat(stackTrace.exceptions()).containsExactly(rootException,
                                                          wrappingException1,
                                                          wrappingException2);

      assertThat(stackTrace.actualException()).isEqualTo(stackTrace.exceptions().get(2));
      checkWrappingChain(stackTrace.exceptions().get(0),
                         stackTrace.exceptions().get(1),
                         stackTrace.exceptions().get(2));
      assertThat(stackTrace.rootCauseException()).isEqualTo(stackTrace.exceptions().get(0));
      checkCauseChain(stackTrace.exceptions().get(2),
                      stackTrace.exceptions().get(1),
                      stackTrace.exceptions().get(0));
   }

   @Test
   public void parseStackTraceWhenAlternativeFormat() {
      StackTrace stackTrace = StackTrace.parse(alternativeStackTrace());
      assertThat(stackTrace).isNotNull();

      List<TracedMethod> rootExceptionTracedMethods =
            asList(new TracedMethod(new Type("d", "AtClass1"), "atMethod1", "AtClass1.java"),
                   new TracedMethod(new Type("pkg", "AtClass2"), "atMethod2", "AtClass2.java", 491));
      StackTraceException rootException = new StackTraceException(new Type("RootException"),
                                                                  "Root message line 1\n"
                                                                  + "root message line 2",
                                                                  rootExceptionTracedMethods);
      List<TracedMethod> causeException1TracedMethods =
            singletonList(new TracedMethod(new Type("d", "AtClass1"), "atMethod1", "AtClass1.java"));

      StackTraceException causeException1 = new StackTraceException(new Type("CauseException1"),
                                                                    "Cause1 message line 1",
                                                                    causeException1TracedMethods);
      List<TracedMethod> causeException2TracedMethods =
            singletonList(new TracedMethod(new Type("AtClass1"), "atMethod1", "AtClass1.java", 392));
      StackTraceException causeException2 = new StackTraceException(new Type("CauseException2"),
                                                                    "Cause2 message line 1\n"
                                                                    + "    cause2 message line 2",
                                                                    causeException2TracedMethods);

      assertThat(stackTrace.exceptions()).containsExactly(rootException, causeException1, causeException2);

      assertThat(stackTrace.actualException()).isEqualTo(stackTrace.exceptions().get(0));
      checkCauseChain(stackTrace.exceptions().get(0),
                      stackTrace.exceptions().get(1),
                      stackTrace.exceptions().get(2));
      assertThat(stackTrace.rootCauseException()).isEqualTo(stackTrace.exceptions().get(2));
      checkWrappingChain(stackTrace.exceptions().get(2),
                         stackTrace.exceptions().get(1),
                         stackTrace.exceptions().get(0));
   }

   @Test
   public void parseStackTraceWhenNoRootChain() {
      assertThatThrownBy(() -> StackTrace.parse(noRootStackTrace()))
            .isInstanceOf(InvalidStackTraceException.class)
            .hasMessageMatching(
                  "Inconsistent exception chain for 'StackTraceException\\[.*\\]' > No root exception");
   }

   private String simpleStackTrace() {
      return "java.pkg.RootException: Root message line 1\n"
             + "    root message line 2\n"
             + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
             + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n"
             + "      ... extra message\n";
   }

   private String causedByStackTrace() {
      return simpleStackTrace()
             + "   Caused by: java.pkg.CauseException1: Cause1 message line 1\n"
             + "    cause1 message line 2\n"
             + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
             + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n"
             + "      ... extra message\n"
             + "   Caused by: java.pkg.CauseException2: Cause2 message line 1\n"
             + "    cause2 message line 2\n"
             + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
             + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n"
             + "      ... extra message\n";
   }

   private String wrappedByStackTrace() {
      return simpleStackTrace()
             + "   Wrapped by: java.pkg.WrappingException1: Wrapping1 message line 1\n"
             + "    wrapping1 message line 2\n"
             + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
             + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n"
             + "      ... extra message\n"
             + "   Wrapped by: java.pkg.WrappingException2: Wrapping2 message line 1\n"
             + "    wrapping2 message line 2\n"
             + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
             + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n"
             + "      ... extra message\n";
   }

   private String alternativeStackTrace() {
      return "RootException:\t    \tRoot message line 1\n"
             + "root message line 2    \n"
             + "      at d.AtClass1.atMethod1(AtClass1.java)\n"
             + "      at pkg.AtClass2.atMethod2(AtClass2.java:491)\n"
             + "      ... extra message that is not standardized\n"
             + "   Caused by: CauseException1: Cause1 message line 1\n"
             + "      at d.AtClass1.atMethod1(AtClass1.java)\n"
             + "   Caused by: CauseException2: Cause2 message line 1\n"
             + "    cause2 message line 2\t    \t\n"
             + "      at AtClass1.atMethod1(AtClass1.java:392)\n"
             + "\n\n";
   }

   private String noRootStackTrace() {
      return "   Caused by: java.pkg.CauseException: Cause message line 1\n"
             + "    cause message line 2\n"
             + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
             + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n"
             + "      ... extra message\n";
   }

   private String unknownElementStackTrace() {
      return simpleStackTrace()
             + "   Unknown by: java.pkg.CauseException: Cause message line 1\n"
             + "    cause message line 2\n"
             + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
             + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n"
             + "      ... extra message\n";
   }

   /**
    * Checks the cause chain from the specified root exception, following only the "caused by" associations.
    * All cause exceptions must be found in order.
    */
   private void checkCauseChain(StackTraceException rootException, StackTraceException... causeExceptions) {
      Optional<StackTraceException> currentException = rootException.causedBy();
      for (StackTraceException causeException : causeExceptions) {

         if (currentException.isPresent()) {
            assertThat(currentException.get()).isEqualTo(causeException);
            currentException = currentException.get().causedBy();
         } else {
            throw new IllegalStateException(String.format(
                  "Cause exception '%s' not found at expected location in chain",
                  causeException));
         }
      }
      assertThat(currentException).isEmpty();
   }

   /**
    * Checks the wrapping chain from the specified root exception, following only the "wrapped by"
    * associations.
    * All wrapping exceptions must be found in order.
    */
   private void checkWrappingChain(StackTraceException rootException,
                                   StackTraceException... wrappingExceptions) {
      Optional<StackTraceException> currentException = rootException.wrappedBy();
      for (StackTraceException wrapping : wrappingExceptions) {

         if (currentException.isPresent()) {
            assertThat(currentException.get()).isEqualTo(wrapping);
            currentException = currentException.get().wrappedBy();
         } else {
            throw new IllegalStateException(String.format(
                  "Wrapping exception '%s' not found at expected location in chain",
                  wrapping));
         }
      }
      assertThat(currentException).isEmpty();
   }

}