/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.stacktrace.visitors;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.tinubu.logexplorer.core.stacktrace.StackTrace;
import com.tinubu.logexplorer.core.stacktrace.StackTraceException;
import com.tinubu.logexplorer.core.stacktrace.TracedMethod;

class AnsiStackTraceVisitorTest {

   @BeforeAll
   public static void disableAnsi() {
      System.setProperty("org.fusesource.jansi.Ansi.disable", "true");
   }

   @Test
   public void outputStringWithRootCauseVisitorWhenSimpleChain() {
      OutputStringVisitor outputStringVisitor = new AnsiStackTraceVisitor();
      StackTrace.parse(simpleStackTrace()).rootCauseExceptionVisitor(outputStringVisitor);
      assertThat(outputStringVisitor.toString()).isEqualTo("java.pkg.RootException: Root message line 1\n"
                                                           + "    root message line 2\n"
                                                           + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
                                                           + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n");

   }

   @Test
   public void outputStringWithActualExceptionVisitorWhenSimpleChain() {
      OutputStringVisitor outputStringVisitor = new AnsiStackTraceVisitor();
      StackTrace.parse(simpleStackTrace()).actualExceptionVisitor(outputStringVisitor);
      assertThat(outputStringVisitor.toString()).isEqualTo("java.pkg.RootException: Root message line 1\n"
                                                           + "    root message line 2\n"
                                                           + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
                                                           + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n");
   }

   @Test
   public void outputStringWithRootCauseVisitorWhenCausedByChain() {
      OutputStringVisitor outputStringVisitor = new AnsiStackTraceVisitor();
      StackTrace.parse(causedByStackTrace()).rootCauseExceptionVisitor(outputStringVisitor);
      assertThat(outputStringVisitor.toString()).isEqualTo(
            "[Root cause] java.pkg.CauseException2: Cause2 message line 1\n"
            + "    cause2 message line 2\n"
            + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
            + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n"
            + "   Wrapped by: java.pkg.CauseException1: Cause1 message line 1\n"
            + "    cause1 message line 2\n"
            + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
            + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n"
            + "   Wrapped by: [Actual exception] java.pkg.RootException: Root message line 1\n"
            + "    root message line 2\n"
            + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
            + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n");
   }

   @Test
   public void outputStringWithActualExceptionVisitorWhenCausedByChain() {
      OutputStringVisitor outputStringVisitor = new AnsiStackTraceVisitor();
      StackTrace.parse(causedByStackTrace()).actualExceptionVisitor(outputStringVisitor);
      assertThat(outputStringVisitor.toString()).isEqualTo(
            "[Actual exception] java.pkg.RootException: Root message line 1\n"
            + "    root message line 2\n"
            + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
            + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n"
            + "   Caused by: java.pkg.CauseException1: Cause1 message line 1\n"
            + "    cause1 message line 2\n"
            + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
            + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n"
            + "   Caused by: [Root cause] java.pkg.CauseException2: Cause2 message line 1\n"
            + "    cause2 message line 2\n"
            + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
            + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n");
   }

   @Test
   public void outputStringWithRootCauseVisitorWhenWrappedByChain() {
      OutputStringVisitor outputStringVisitor = new AnsiStackTraceVisitor();
      StackTrace.parse(wrappedByStackTrace()).rootCauseExceptionVisitor(outputStringVisitor);
      assertThat(outputStringVisitor.toString()).isEqualTo(
            "[Root cause] java.pkg.RootException: Root message line 1\n"
            + "    root message line 2\n"
            + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
            + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n"
            + "   Wrapped by: java.pkg.WrappingException1: Wrapping1 message line 1\n"
            + "    wrapping1 message line 2\n"
            + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
            + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n"
            + "   Wrapped by: [Actual exception] java.pkg.WrappingException2: Wrapping2 message line 1\n"
            + "    wrapping2 message line 2\n"
            + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
            + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n");
   }

   @Test
   public void outputStringWithActualExceptionVisitorWhenWrappedByChain() {
      OutputStringVisitor outputStringVisitor = new AnsiStackTraceVisitor();
      StackTrace.parse(wrappedByStackTrace()).actualExceptionVisitor(outputStringVisitor);
      assertThat(outputStringVisitor.toString()).isEqualTo(
            "[Actual exception] java.pkg.WrappingException2: Wrapping2 message line 1\n"
            + "    wrapping2 message line 2\n"
            + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
            + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n"
            + "   Caused by: java.pkg.WrappingException1: Wrapping1 message line 1\n"
            + "    wrapping1 message line 2\n"
            + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
            + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n"
            + "   Caused by: [Root cause] java.pkg.RootException: Root message line 1\n"
            + "    root message line 2\n"
            + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
            + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n");
   }

   @Test
   public void outputStringWhenCustomOutputStringVisitor() {
      OutputStringVisitor outputStringVisitor = new CustomOutputStringVisitor();
      StackTrace.parse(wrappedByStackTrace()).rootCauseExceptionVisitor(outputStringVisitor);
      assertThat(outputStringVisitor.toString()).isEqualTo(
            "@[Root cause] java.pkg.RootException: @Root message line 1\n"
            + "    root message line 2\n"
            + "      @at @o.a.c.d.AtClass1.@atMethod1(@AtClass1.java:@392)\n"
            + "   @Wrapped by: @java.pkg.WrappingException1: @Wrapping1 message line 1\n"
            + "    wrapping1 message line 2\n"
            + "      @at @o.a.c.d.AtClass1.@atMethod1(@AtClass1.java:@392)\n"
            + "   @Wrapped by: @[Actual exception] java.pkg.WrappingException2: @Wrapping2 message line 1\n"
            + "    wrapping2 message line 2\n"
            + "      @at @o.a.c.d.AtClass1.@atMethod1(@AtClass1.java:@392)\n");
   }

   private String simpleStackTrace() {
      return "java.pkg.RootException: Root message line 1\n"
             + "    root message line 2\n"
             + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
             + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n"
             + "      ... extra message\n";
   }

   private String causedByStackTrace() {
      return simpleStackTrace()
             + "   Caused by: java.pkg.CauseException1: Cause1 message line 1\n"
             + "    cause1 message line 2\n"
             + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
             + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n"
             + "      ... extra message\n"
             + "   Caused by: java.pkg.CauseException2: Cause2 message line 1\n"
             + "    cause2 message line 2\n"
             + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
             + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n"
             + "      ... extra message\n";
   }

   private String wrappedByStackTrace() {
      return simpleStackTrace()
             + "   Wrapped by: java.pkg.WrappingException1: Wrapping1 message line 1\n"
             + "    wrapping1 message line 2\n"
             + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
             + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n"
             + "      ... extra message\n"
             + "   Wrapped by: java.pkg.WrappingException2: Wrapping2 message line 1\n"
             + "    wrapping2 message line 2\n"
             + "      at o.a.c.d.AtClass1.atMethod1(AtClass1.java:392)\n"
             + "      at org.pkg.AtClass2.atMethod2(AtClass2.java:491)\n"
             + "      ... extra message\n";
   }

   private static class CustomOutputStringVisitor extends AnsiStackTraceVisitor {
      @Override
      protected String filterExceptionAssociation(StackTraceException exception,
                                                  String exceptionAssociation) {
         return "@" + super.filterExceptionAssociation(exception, exceptionAssociation);
      }

      @Override
      protected String filterExceptionMessage(StackTraceException exception, String exceptionMessage) {
         return "@" + super.filterExceptionMessage(exception, exceptionMessage);
      }

      @Override
      protected String filterExceptionType(StackTraceException exception, String exceptionType) {
         return "@" + super.filterExceptionType(exception, exceptionType);
      }

      @Override
      protected String filterTracedMethodPrefix(StackTraceException exception,
                                                TracedMethod tracedMethod,
                                                String prefix) {
         return "@" + prefix;
      }

      @Override
      protected String filterTracedMethodClassName(StackTraceException exception,
                                                   TracedMethod tracedMethod,
                                                   String className) {
         return "@" + super.filterTracedMethodClassName(exception, tracedMethod, className);
      }

      @Override
      protected String filterTracedMethodName(StackTraceException exception,
                                              TracedMethod tracedMethod,
                                              String methodName) {
         return "@" + super.filterTracedMethodName(exception, tracedMethod, methodName);
      }

      @Override
      protected String filterTracedMethodFileName(StackTraceException exception,
                                                  TracedMethod tracedMethod,
                                                  String fileName) {
         return "@" + super.filterTracedMethodFileName(exception, tracedMethod, fileName);
      }

      @Override
      protected String filterTracedMethodLine(StackTraceException exception,
                                              TracedMethod tracedMethod,
                                              int methodLine) {
         return "@" + super.filterTracedMethodLine(exception, tracedMethod, methodLine);
      }

      @Override
      protected boolean filterTracedMethod(StackTraceException exception, TracedMethod tracedMethod) {
         return tracedMethod.methodName().equals("atMethod1");
      }
   }
}