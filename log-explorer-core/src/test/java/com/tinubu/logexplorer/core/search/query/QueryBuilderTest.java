/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.search.query;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.logexplorer.core.search.query.UniformQuery.Operator.EQUAL;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.regex.Pattern;

import org.junit.jupiter.api.Test;

import com.tinubu.logexplorer.core.search.query.UniformQuery.NullOperand;
import com.tinubu.logexplorer.core.search.query.UniformQuery.Operator;

class QueryBuilderTest {

   @Test
   public void testParseSimpleQueryWhenNominal() {
      assertThat(QueryBuilder.parseQuery("/search")).isEqualTo(SimpleQuery.of("search"));
   }

   @Test
   public void testParseSimpleQueryWhenAlternativeSelector() {
      assertThat(QueryBuilder.parseQuery("s/search")).isEqualTo(SimpleQuery.of("search"));
      assertThat(QueryBuilder.parseQuery("simple/search")).isEqualTo(SimpleQuery.of("search"));
   }

   @Test
   public void testParseUniformQueryWhenNominal() {
      assertThat(QueryBuilder.parseQuery("u/field=\"32\"")).isEqualTo(UniformQuery.fieldQuery(singletonList(
            "field"), EQUAL, "32"));
   }

   @Test
   public void testParseUniformQueryWhenAlternativeSelector() {
      assertThat(QueryBuilder.parseQuery("uniform/field=\"32\"")).isEqualTo(UniformQuery.fieldQuery(
            singletonList("field"),
            EQUAL,
            "32"));
   }

   @Test
   public void testParseUniformQueryWhenTypedOperand() {
      assertThat(QueryBuilder.parseQuery("u/field=\"32\"")).isEqualTo(UniformQuery.fieldQuery(singletonList(
            "field"), Operator.EQUAL, "32"));
      assertThat(QueryBuilder.parseQuery("u/field=32")).isEqualTo(UniformQuery.fieldQuery(singletonList(
            "field"), Operator.EQUAL, 32L));
      assertThat(QueryBuilder.parseQuery("u/field=32.0")).isEqualTo(UniformQuery.fieldQuery(singletonList(
            "field"), Operator.EQUAL, 32.0d));
      assertThat(QueryBuilder.parseQuery("u/field=true")).isEqualTo(UniformQuery.fieldQuery(singletonList(
            "field"), Operator.EQUAL, true));
      assertThat(QueryBuilder.parseQuery("u/field=/32/")).isEqualTo(UniformQuery.fieldQuery(singletonList(
            "field"), Operator.EQUAL, Pattern.compile("32")));
      assertThatThrownBy(() -> QueryBuilder.parseQuery("u/field=a string"))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("Can't parse 'u/field=a string' query");
   }

   @Test
   public void testParseQueryWhenUniformWithoutField() {
      assertThat(QueryBuilder.parseQuery("u/=\"32\"")).isEqualTo(UniformQuery.anyFieldQuery(Operator.EQUAL,
                                                                                            "32"));
      assertThat(QueryBuilder.parseQuery("u/=32")).isEqualTo(UniformQuery.anyFieldQuery(Operator.EQUAL, 32L));
      assertThat(QueryBuilder.parseQuery("u/=32.0")).isEqualTo(UniformQuery.anyFieldQuery(Operator.EQUAL,
                                                                                          32.0d));
      assertThat(QueryBuilder.parseQuery("u/=true")).isEqualTo(UniformQuery.anyFieldQuery(Operator.EQUAL,
                                                                                          true));
      assertThat(QueryBuilder.parseQuery("u/=/32/")).isEqualTo(UniformQuery.anyFieldQuery(Operator.EQUAL,
                                                                                          Pattern.compile("32")));

   }

   @Test
   public void testParseQueryWhenUniformWithNullOperand() {
      assertThat(QueryBuilder.parseQuery("u/attribute=null")).isEqualTo(UniformQuery.fieldQuery(singletonList(
            "attribute"), Operator.EQUAL, new NullOperand()));
   }

   @Test
   public void testParseUniformQueryWhenDottedAttribute() {
      assertThat(QueryBuilder.parseQuery("u/field.dotted\\.field=\"value\"")).isEqualTo(UniformQuery.fieldQuery(
            list("field", "dotted.field"),
            Operator.EQUAL,
            "value"));
   }

   @Test
   public void testParseUniformQueryWhenEscapedQuotes() {
      assertThat(QueryBuilder.parseQuery("u/field=\"separated\\\"value\"")).isEqualTo(UniformQuery.fieldQuery(
            singletonList("field"),
            Operator.EQUAL,
            "separated\"value"));
   }

   @Test
   public void testParseQueryWhenUniformComposition() {
      assertThat(QueryBuilder.parseQuery("u/!=32")).isEqualTo(UniformQuery.anyFieldQuery(Operator.NOT_EQUAL,
                                                                                         32L));
      assertThat(QueryBuilder.parseQuery("u/((=32))")).isEqualTo(UniformQuery.anyFieldQuery(Operator.EQUAL,
                                                                                            32L));
      assertThatExceptionOfType(UnsupportedOperationException.class).isThrownBy(() -> QueryBuilder.parseQuery(
            "u/! =32"));
      assertThatExceptionOfType(UnsupportedOperationException.class).isThrownBy(() -> QueryBuilder.parseQuery(
            "u/=32 && =43"));
      assertThatExceptionOfType(UnsupportedOperationException.class).isThrownBy(() -> QueryBuilder.parseQuery(
            "u/=32 || =43"));
   }

}