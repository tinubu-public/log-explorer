/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.file;

import static com.tinubu.commons.lang.util.StringUtils.isBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;
import static java.nio.charset.StandardCharsets.ISO_8859_1;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Predicate;

import com.tinubu.commons.lang.log.ExtendedLogger;
import com.tinubu.logexplorer.core.backend.Backend;
import com.tinubu.logexplorer.core.backend.BackendConfiguration;
import com.tinubu.logexplorer.core.backend.BackendInterruptionState;
import com.tinubu.logexplorer.core.config.Configuration.Parameters;
import com.tinubu.logexplorer.core.search.QueryDateRange;
import com.tinubu.logexplorer.core.search.SearchResult;
import com.tinubu.logexplorer.core.search.SearchResult.Attributes;
import com.tinubu.logexplorer.core.search.query.QuerySpecification;

/**
 * File backend.
 * <p>
 * By default, UTF-8 file encoding is assumed.
 * <p>
 * Handles :
 * <ul>
 *    <li>large files, even if {@code numberlines} is defined</li>
 *    <li>file truncate in follow mode</li>
 * </ul>
 */
public class FileBackend implements Backend {

   /** Wait duration in follow mode between each lookup. */
   private static final Duration FOLLOW_WAIT_DURATION = Duration.ofMillis(500);
   /** Single attribute name for each line. */
   private static final String LINE_ATTRIBUTE = "message";
   /**
    * Max lines to read before calling action.
    * Helps to :
    * <ul>
    *    <li>minimize memory consumption</li>
    *    <li>to parallelize read/display operations instead of reading the whole file before displaying it</li>
    *    <li>Diminishes display latency</li>
    * </ul>
    * This optimization is not always possible e.g. if numberLines is enabled.
    */
   private static final int MAX_LINES_PER_ACTION = 200;
   /** Assumed file encoding. */
   // TODO : move to user parameter
   private static final Charset FILE_ENCODING = UTF_8;

   private static final ExtendedLogger logger = ExtendedLogger.of(FileBackend.class);

   /**
    * File patch or {@code null} to read from stdin.
    */
   private final Path file;

   public FileBackend(Parameters parameters, BackendConfiguration configuration) {
      notNull(parameters, "parameters");
      notNull(configuration, "configuration");
      satisfies(configuration.backendUris(),
                uris -> uris != null && uris.size() == 1,
                "backendUris",
                "must contain one and only one URI");
      satisfies(configuration.backendUris().get(0),
                isValidUri(),
                "backendUris[0]", "must be at format '-' or '<path>' or 'file://</absolute-path>'");

      file = configuration.backendUris().get(0).getPath().equals("-")
             ? null
             : Path.of(configuration.backendUris().get(0).getPath());

      if (file != null) {
         if (!file.toFile().canRead()) {
            throw new IllegalStateException(String.format("Can't read from '%s'", file));
         }
      }

      logger.debug("Search from '{}'", () -> file);
   }

   private static Predicate<URI> isValidUri() {
      return uri -> !uri.isAbsolute() || (uri.getScheme().equals("file") && !isBlank(uri.getPath()));
   }

   @Override
   public void search(BiConsumer<SearchResult, QuerySpecification> action,
                      QuerySpecification querySpecification,
                      QueryDateRange dateRange,
                      Long numberLines,
                      boolean followMode) {
      if (numberLines != null) {
         satisfies(numberLines, nbl -> nbl >= 0, "numberLines", "must be >= 0");
      }

      if (file == null) {
         readFromInputStream(System.in, action, querySpecification, numberLines, followMode);
      } else {
         readFromFile(file.toFile(), action, querySpecification, numberLines, followMode);
      }

   }

   /**
    * Reads from a file.
    * {@code numberLines} has no performance impact if input is large, as the content is read from end of the
    * file.
    *
    * @param file file input
    * @param action action to call for results
    * @param querySpecification query specification
    * @param numberLines max number of lines
    * @param followMode follow mode
    */
   private void readFromFile(File file,
                             BiConsumer<SearchResult, QuerySpecification> action,
                             QuerySpecification querySpecification,
                             Long numberLines,
                             boolean followMode) {
      try (RandomAccessFile raf = new RandomAccessFile(file, "r")) {

         long fileLength = raf.length();
         boolean maxLinesPerActionReached;

         do {
            maxLinesPerActionReached = false;
            String line;
            List<Attributes> results = new ArrayList<>();
            while (!maxLinesPerActionReached && (line = readLine(raf, FILE_ENCODING)) != null) {
               results.add(mapLine(line));

               maxLinesPerActionReached = numberLines == null && maxLinesPerActionReached(results);
            }

            // FIXME skip number lines from file END using seek and bufferization (caution with UTF-8 encoding and seek alignments in bytes :/)
            if (numberLines != null) {
               long r = 0;
               long s = results.size() - numberLines;
               for (Iterator<Attributes> it = results.iterator(); it.hasNext() && r < s; r++) {
                  it.next();
                  it.remove();
               }
               numberLines = null;
            }

            action.accept(new SearchResult(results), querySpecification);

            if (followMode) {
               try {
                  Thread.sleep(FOLLOW_WAIT_DURATION.toMillis());
               } catch (InterruptedException e) {
                  Thread.currentThread().interrupt();
               }

               if (fileLength > file.length()) {
                  raf.seek(0);
                  fileLength = file.length();
               } else {
                  fileLength = file.length();
               }
            }
         } while (!BackendInterruptionState.interrupting() && (maxLinesPerActionReached || followMode));

      } catch (IOException e) {
         throw new IllegalStateException(e);
      }

   }

   /**
    * Reads from a generic {@link InputStream}.
    * {@code numberLines} has a performance impact if input is large, as the whole input must be read.
    *
    * @param inputStream input
    * @param action action to call for results
    * @param querySpecification query specification
    * @param numberLines max number of lines
    * @param followMode follow mode
    */
   private void readFromInputStream(InputStream inputStream,
                                    BiConsumer<SearchResult, QuerySpecification> action,
                                    QuerySpecification querySpecification,
                                    Long numberLines,
                                    boolean followMode) {
      try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {

         boolean maxLinesPerActionReached;

         do {
            maxLinesPerActionReached = false;
            String line;
            List<Attributes> results = new ArrayList<>();
            while (!maxLinesPerActionReached && (line = reader.readLine()) != null) {
               results.add(mapLine(line));
               maxLinesPerActionReached = numberLines == null && maxLinesPerActionReached(results);
            }

            if (numberLines != null) {
               long r = 0;
               long s = results.size() - numberLines;
               for (Iterator<Attributes> it = results.iterator(); it.hasNext() && r < s; r++) {
                  it.next();
                  it.remove();
               }
               numberLines = null;
            }

            action.accept(new SearchResult(results), querySpecification);

            if (!maxLinesPerActionReached && followMode) {
               try {
                  Thread.sleep(FOLLOW_WAIT_DURATION.toMillis());
               } catch (InterruptedException e) {
                  Thread.currentThread().interrupt();
               }
            }
         } while (!BackendInterruptionState.interrupting() && (maxLinesPerActionReached || followMode));
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }

   }

   // FIXME RandomAccessFile::readline is not buffered
   private static String readLine(RandomAccessFile raf, Charset encoding) throws IOException {
      String line = raf.readLine();

      if (line == null) {
         return null;
      }

      return new String(line.getBytes(ISO_8859_1), encoding);
   }

   private static boolean maxLinesPerActionReached(List<Attributes> results) {
      return results.size() >= MAX_LINES_PER_ACTION;
   }

   private Attributes mapLine(String line) {
      return new Attributes().withAttribute(LINE_ATTRIBUTE, line);
   }

   @Override
   public void close() {
   }

}
