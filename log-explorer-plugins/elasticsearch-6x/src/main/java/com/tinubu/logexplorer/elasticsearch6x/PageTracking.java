/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.elasticsearch6x;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;

import com.tinubu.logexplorer.core.search.QueryDateRange;
import com.tinubu.logexplorer.core.search.query.QuerySpecification;

/**
 * Tracks state between page requests.
 */
public class PageTracking {
   private final QuerySpecification querySpecification;
   private final QueryDateRange dateRange;
   private final Long numberLines;
   private final boolean followMode;
   /** True if an initial response has been set. */
   private boolean initialResponse = false;
   /** Current tracked response value. */
   private SearchResponse trackedResponse;
   /** Last non empty sort values to use to follow requests. */
   private Object[] trackedSortValues;

   /**
    * Current tracked number of remaining lines to display, or
    * {@code null} if lines are not restricted.
    */
   private Long trackedNumberLines;
   private final Set<String> trackedScrollIds = new HashSet<>();

   public PageTracking(QuerySpecification querySpecification,
                       QueryDateRange dateRange,
                       Long numberLines,
                       boolean followMode) {
      this.querySpecification = querySpecification;
      this.dateRange = dateRange;
      this.numberLines = numberLines;
      this.followMode = followMode;
   }

   public QuerySpecification querySet() {
      return querySpecification;
   }

   public ZonedDateTime fromDate() {
      return dateRange.fromDate();
   }

   /** Returns to date for requests. In follow mode, we never use a to date. */
   public ZonedDateTime toDate() {
      return followMode ? null : dateRange.toDate();
   }

   public Long numberLines() {
      return numberLines;
   }

   public boolean followMode() {
      return followMode;
   }

   /** Start tracking without initial response (e.g.: scroll request, search after in regular mode). */
   public void initializeTracking() {
      initializeTracking(null);
   }

   /**
    * Start tracking with an initial response which is *not* part of the real paginated request (e.g.: search
    * after with limited number of lines to retrieve the correct starting point).
    */
   public void initializeTracking(SearchResponse response) {
      trackedResponse = response;
      initialResponse = (response != null);
      trackedNumberLines = numberLines;
      trackedSortValues = lastHit().map(SearchHit::getSortValues).orElse(null);
   }

   /**
    * Tracks page responses.
    */
   public void trackResponse(SearchResponse response) {
      trackedResponse = response;
      initialResponse = false;
      if (response.getScrollId() != null) {
         trackedScrollIds.add(response.getScrollId());
      }
      if (trackedNumberLines != null) {
         trackedNumberLines -= response.getHits().getHits().length;
      }
      lastHit().ifPresent(lastHit -> trackedSortValues = lastHit.getSortValues());
   }

   /**
    * Returns true, in the general case, if we should continue requests given the last tracked response and
    * current state.
    */
   public boolean shouldRequestNextPage() {
      return followMode || (((trackedResponse == null || lastHit().isPresent())
                             || initialResponseOutOfBounds()) && linesNumberNotReached());
   }

   public boolean linesNumberNotReached() {
      return trackedNumberLines == null || trackedNumberLines > 0;
   }

   /**
    * Returns the maximum number of remaining lines to display or {@link Optional#empty()} if all lines can be
    * displayed without restriction.
    */
   public Optional<Long> remainingLinesNumber() {
      Optional<Long> remainingLines = Optional.empty();
      if (!followMode && trackedNumberLines != null && trackedNumberLines <= 0) {
         remainingLines = Optional.of(trackedNumberLines + trackedResponse.getHits().getHits().length);
      }
      return remainingLines;
   }

   /**
    * Returns true if *the initial request response* and, only this one, didn't return any hit but some
    * values are available (total hits > 0).
    * It is used, for some use cases, when a request with a from index greater than the number of values
    * available returns no hit but some values are still available.
    */
   public boolean initialResponseOutOfBounds() {
      return initialResponse && lastHit().isEmpty() && trackedResponse.getHits().getTotalHits() > 0;
   }

   /**
    * Returns the sort values the for next page query.
    *
    * @implSpec In follow mode, the next sort values must not be only the last response last values
    *       which can be {@code null}, but the last valid/known sort values.
    */
   public Object[] nextPageSortValues() {
      return trackedSortValues;
   }

   /**
    * Returns the tracked responses's last hit or {@link Optional#empty()} if no tracked response or no hits.
    */
   public Optional<SearchHit> lastHit() {
      if (trackedResponse != null && trackedResponse.getHits().getHits().length > 0) {
         return Optional.of(trackedResponse.getHits().getAt(trackedResponse.getHits().getHits().length - 1));
      } else {
         return Optional.empty();
      }
   }

   public SearchResponse trackedResponse() {
      return trackedResponse;
   }

   public List<String> trackedScrollIds() {
      return new ArrayList<>(trackedScrollIds);
   }
}
