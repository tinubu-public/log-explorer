/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.elasticsearch6x;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;
import static com.tinubu.logexplorer.core.search.query.QueryBuilder.nativeQuery;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.function.BiConsumer;

import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.client.RequestOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.logexplorer.core.backend.BackendConfiguration;
import com.tinubu.logexplorer.core.backend.BackendInterruptionState;
import com.tinubu.logexplorer.core.config.Configuration.Parameters;
import com.tinubu.logexplorer.core.search.QueryDateRange;
import com.tinubu.logexplorer.core.search.SearchResult;
import com.tinubu.logexplorer.core.search.query.NativeQuery;
import com.tinubu.logexplorer.core.search.query.QuerySpecification;

public class AsyncElasticsearchBackend extends AbstractElasticsearchBackend {

   private static final Logger logger = LoggerFactory.getLogger(AsyncElasticsearchBackend.class);

   private final CountDownLatch asyncRequestLatch = new CountDownLatch(1);

   private BiConsumer<SearchResult, QuerySpecification> action;
   private PageTracking pageTracking;

   public AsyncElasticsearchBackend(Parameters parameters,
                                    BackendConfiguration configuration,
                                    boolean debug) {
      super(parameters, configuration, debug);
   }

   public void search(BiConsumer<SearchResult, QuerySpecification> action,
                      QuerySpecification querySpecification,
                      QueryDateRange dateRange,
                      Long numberLines,
                      boolean followMode) {
      this.action = notNull(action, "action");
      notNull(querySpecification, "querySet");
      if (querySpecification.querySet(NativeQuery.class).isEmpty() && querySpecification
            .notQuerySet(NativeQuery.class)
            .isEmpty()) {
         querySpecification = querySpecification.addQuery(nativeQuery("*"));
      }
      if (numberLines != null) {
         satisfies(numberLines, nbl -> nbl >= 0, "numberLines", "must be >= 0");
      }

      logger.debug("Search Elasticsearch for query set '{}' spanning '{}' (numberLines={} follow={})",
                   querySpecification,
                   dateRange,
                   numberLines,
                   followMode);

      pageTracking = new PageTracking(querySpecification, dateRange, numberLines, followMode);

      if (followMode || numberLines != null) {
         searchAfterFirstPage();
      } else {
         scrollFirstPage();
      }
      try {
         asyncRequestLatch.await();
      } catch (InterruptedException e) {
         Thread.currentThread().interrupt();
      } finally {
         clearScrollContext(pageTracking.trackedScrollIds());
      }
   }

   private void scrollFirstPage() {
      pageTracking.initializeTracking();

      if (!BackendInterruptionState.interrupting() && pageTracking.shouldRequestNextPage()) {
         client.searchAsync(searchScrollRequest(pageTracking.querySet(),
                                                pageTracking.fromDate(),
                                                pageTracking.toDate()),
                            RequestOptions.DEFAULT,
                            new ScrollListener());

      } else {
         asyncRequestLatch.countDown();
      }
   }

   private SearchResponse searchNth(int nth) {
      try {
         return client.search(searchNthRequest(pageTracking.querySet(),
                                               pageTracking.fromDate(),
                                               pageTracking.toDate(),
                                               nth), RequestOptions.DEFAULT);
      } catch (IOException e) {
         throw new IllegalStateException(String.format("Error using '%s'", configuration.backendUris()), e);
      }
   }

   private void searchAfterFirstPage() {
      if (pageTracking.numberLines() != null) {
         pageTracking.initializeTracking(searchNth(pageTracking.numberLines().intValue()));
      } else {
         pageTracking.initializeTracking();
      }

      if (!BackendInterruptionState.interrupting() && pageTracking.shouldRequestNextPage()) {
         client.searchAsync(searchAfterRequest(pageTracking.querySet(),
                                               pageTracking.fromDate(),
                                               pageTracking.toDate(),
                                               pageTracking.nextPageSortValues()),
                            RequestOptions.DEFAULT,
                            new SearchAfterListener());
      } else {
         asyncRequestLatch.countDown();
      }
   }

   private abstract class AbstractListener implements ActionListener<SearchResponse> {

      @Override
      public synchronized void onFailure(Exception e) {
         System.err.println("[E] " + e.getClass().getSimpleName() + " : " + e.getMessage());
         if (logger.isDebugEnabled()) {
            logger.debug(e.getMessage(), e);
         }
         asyncRequestLatch.countDown();
      }
   }

   private class ScrollListener extends AbstractListener {

      @Override
      public synchronized void onResponse(SearchResponse searchResponse) {
         try {
            pageTracking.trackResponse(searchResponse);

            if (!BackendInterruptionState.interrupting() && pageTracking.shouldRequestNextPage()) {
               searchNextPage(searchResponse.getScrollId());
               callAction(action,
                          pageTracking.querySet(),
                          searchResponse,
                          pageTracking.remainingLinesNumber().orElse(null));
               if (pageTracking.followMode()) {
                  try {
                     Thread.sleep(followWaitDuration);
                  } catch (InterruptedException e) {
                     Thread.currentThread().interrupt();
                  }
               }
            } else {
               callAction(action,
                          pageTracking.querySet(),
                          searchResponse,
                          pageTracking.remainingLinesNumber().orElse(null));
               asyncRequestLatch.countDown();
            }
         } catch (Exception e) {
            onFailure(e);
         }
      }

      private void searchNextPage(String scrollId) {
         SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
         scrollRequest.scroll(SCROLL_TIMEOUT);

         client.scrollAsync(scrollRequest, RequestOptions.DEFAULT, new ScrollListener());
      }
   }

   private class SearchAfterListener extends AbstractListener {

      @Override
      public synchronized void onResponse(SearchResponse searchResponse) {
         try {
            pageTracking.trackResponse(searchResponse);

            if (!BackendInterruptionState.interrupting() && pageTracking.shouldRequestNextPage()) {
               searchNextPage(pageTracking.nextPageSortValues());
               callAction(action,
                          pageTracking.querySet(),
                          searchResponse,
                          pageTracking.remainingLinesNumber().orElse(null));
               if (pageTracking.followMode()) {
                  Thread.sleep(followWaitDuration);
               }
            } else {
               callAction(action,
                          pageTracking.querySet(),
                          searchResponse,
                          pageTracking.remainingLinesNumber().orElse(null));
               asyncRequestLatch.countDown();
            }
         } catch (Exception e) {
            onFailure(e);
         }
      }

      private void searchNextPage(Object[] searchNextValues) {
         client.searchAsync(searchAfterRequest(pageTracking.querySet(),
                                               pageTracking.fromDate(),
                                               pageTracking.toDate(),
                                               searchNextValues), RequestOptions.DEFAULT, this);
      }

   }
}
