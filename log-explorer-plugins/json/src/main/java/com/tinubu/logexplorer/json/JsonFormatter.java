/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.json;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.stream.Collectors.toMap;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.ser.ZonedDateTimeSerializer;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.logexplorer.core.config.Configuration.Parameters;
import com.tinubu.logexplorer.core.extension.parameter.ParameterDescription;
import com.tinubu.logexplorer.core.extension.parameter.completers.BooleanCompleter;
import com.tinubu.logexplorer.core.formatter.Formatter;
import com.tinubu.logexplorer.core.search.SearchResult.Attributes;

// FIXME why ZonedDateTime are formatted 2 different ways ?
// FIXME why only ZonedDateTime ?
// FIXME timezone not applied in ObjectMapper tuning ?
public class JsonFormatter implements Formatter {

   /** Date pattern parameter definition. */
   private static final Pair<String, String> DATE_FORMAT_PARAMETER =
         Pair.of("json.formatter.date-format", "yyyy-MM-dd'T'HH:mm:ssXXX");
   /** Pretty parameter definition. */
   private static final Pair<String, Boolean> PRETTY_PARAMETER = Pair.of("json.formatter.pretty", true);

   private final ZoneId timezone;
   private final boolean pretty;
   private final DateTimeFormatter dateTimeFormatter;

   public JsonFormatter(Parameters parameters, ZoneId timezone) {
      notNull(parameters, "parameters");
      this.timezone = notNull(timezone, "timezone");
      this.pretty = parameters.booleanValue(PRETTY_PARAMETER.getKey()).orElse(PRETTY_PARAMETER.getValue());
      this.dateTimeFormatter = DateTimeFormatter.ofPattern(parameters
                                                                 .stringValue(DATE_FORMAT_PARAMETER.getKey())
                                                                 .orElse(DATE_FORMAT_PARAMETER.getValue()));
   }

   @Override
   public void format(StringBuilder stringBuilder, Attributes attributes) {
      stringBuilder.append(formatMap(attributes));
   }

   private String formatMap(Map<String, Object> attributes) {
      try {
         ObjectWriter objectWriter;
         if (this.pretty) {
            objectWriter = objectMapper().writerWithDefaultPrettyPrinter();
         } else {
            objectWriter = objectMapper().writer();
         }
         return objectWriter.writeValueAsString(attributes
                                                      .entrySet()
                                                      .stream()
                                                      .sorted(this::attributeComparator)
                                                      .map(this::applyTimezone)
                                                      .collect(toMap(Entry::getKey, Entry::getValue)));
      } catch (JsonProcessingException e) {
         throw new IllegalStateException(e);
      }
   }

   private ObjectMapper objectMapper() {
      JavaTimeModule timeModule = new JavaTimeModule();

      timeModule.addSerializer(ZonedDateTime.class, new ZonedDateTimeSerializer(dateTimeFormatter));

      return new ObjectMapper().registerModule(timeModule);
   }

   /**
    * Apply configuration timezone to {@link ZonedDateTime} attributes.
    */
   private Entry<String, Object> applyTimezone(Entry<String, Object> attribute) {
      if (attribute.getValue() instanceof ZonedDateTime) {
         attribute.setValue(this.dateTimeFormatter.format(((ZonedDateTime) attribute.getValue()).withZoneSameInstant(
               timezone)));
      }
      return attribute;
   }

   /**
    * Use natural attribute key order, except if attribute value is a {@link ZonedDateTime}, then give it
    * higher priority.
    */
   private int attributeComparator(Entry<String, Object> a1, Entry<String, Object> a2) {
      if (a1.getValue() instanceof ZonedDateTime) {
         return -1;
      } else if (a2.getValue() instanceof ZonedDateTime) {
         return 1;
      } else {
         return a1.getKey().compareTo(a2.getKey());
      }
   }

   @Override
   public List<ParameterDescription<?>> parameters() {
      return Arrays.asList(new ParameterDescription<>(DATE_FORMAT_PARAMETER.getKey(),
                                                      String.class,
                                                      DATE_FORMAT_PARAMETER.getValue(),
                                                      "date time formatter").completer(new BooleanCompleter()),
                           new ParameterDescription<>(PRETTY_PARAMETER.getKey(),
                                                      Boolean.class,
                                                      PRETTY_PARAMETER.getValue(),
                                                      "pretty print mode").completer(new BooleanCompleter()));
   }

   @Override
   public String displayString() {
      return JsonFormatterService.FORMATTER_NAME;
   }

   @Override
   public String toString() {
      return new ToStringBuilder(this)
            .append("timezone", timezone)
            .append("pretty", pretty)
            .append("dateTimeFormatter", dateTimeFormatter)
            .toString();
   }
}
