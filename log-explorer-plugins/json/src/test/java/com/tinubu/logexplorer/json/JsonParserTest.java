/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.json;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import com.tinubu.logexplorer.core.config.Configuration.Parameters;
import com.tinubu.logexplorer.core.search.SearchResult;
import com.tinubu.logexplorer.core.search.SearchResult.Attribute;
import com.tinubu.logexplorer.core.search.SearchResult.Attributes;

class JsonParserTest {

   @Test
   public void testParseWhenNoParameters() {
      JsonParser jsonParser = new JsonParser(new Parameters());

      SearchResult result = newResult(attributes("log", "{\"message\":\"log-message\"}"));

      SearchResult parseResult = jsonParser.parse(result);

      assertThat(parseResult.total()).isEqualTo(1);
      assertThat(parseResult.results()).hasSize(1);
      assertThat(parseResult.results().get(0)).isEqualTo(attributes("log", "{\"message\":\"log-message\"}"));
   }

   @Test
   public void testParseWhenParseKeys() {
      JsonParser jsonParser = new JsonParser(new Parameters().withParameter("json.parser.parse-keys", "log"));

      SearchResult result = newResult(attributes("log", "{\"message\":\"log-message\"}"));

      SearchResult parseResult = jsonParser.parse(result);

      assertThat(parseResult.total()).isEqualTo(1);
      assertThat(parseResult.results()).hasSize(1);
      assertThat(parseResult.results().get(0)).isEqualTo(attributes("log",
                                                                    attributes("message", "log-message")));
   }

   @Test
   public void testParseWhenParseKeysWithInvalidJson() {
      JsonParser jsonParser = new JsonParser(new Parameters().withParameter("json.parser.parse-keys", "log"));

      SearchResult result = newResult(attributes("log", "{invalid-json"));

      SearchResult parseResult = jsonParser.parse(result);

      assertThat(parseResult.total()).isEqualTo(1);
      assertThat(parseResult.results()).hasSize(1);
      assertThat(parseResult.results().get(0)).isEqualTo(attributes("log", "{invalid-json"));
   }

   @Test
   public void testParseWhenParseKeysWithTypedValue() {
      JsonParser jsonParser = new JsonParser(new Parameters().withParameter("json.parser.parse-keys", "log"));

      SearchResult result = newResult(attributes("log", 32));

      SearchResult parseResult = jsonParser.parse(result);

      assertThat(parseResult.total()).isEqualTo(1);
      assertThat(parseResult.results()).hasSize(1);
      assertThat(parseResult.results().get(0)).isEqualTo(attributes("log", 32));
   }

   @Test
   public void testParseWhenUnknownParseKeys() {
      JsonParser jsonParser =
            new JsonParser(new Parameters().withParameter("json.parser.parse-keys", "unknown"));

      SearchResult result = newResult(attributes("log", "{\"message\":\"log-message\"}"));

      SearchResult parseResult = jsonParser.parse(result);

      assertThat(parseResult.total()).isEqualTo(1);
      assertThat(parseResult.results()).hasSize(1);
      assertThat(parseResult.results().get(0)).isEqualTo(attributes("log", "{\"message\":\"log-message\"}"));
   }

   @Test
   public void testParseWhenMultipleParseKeys() {
      JsonParser jsonParser =
            new JsonParser(new Parameters().withParameter("json.parser.parse-keys", "log1,log2"));

      SearchResult result = newResult(attributes(Attribute.of("log1", "{\"message1\":\"log-message\"}"),
                                                 Attribute.of("log2", "{\"message2\":\"log-message\"}")));

      SearchResult parseResult = jsonParser.parse(result);

      assertThat(parseResult.total()).isEqualTo(1);
      assertThat(parseResult.results()).hasSize(1);
      assertThat(parseResult.results().get(0)).isEqualTo(attributes(Attribute.of("log1",
                                                                                 attributes("message1",
                                                                                            "log-message")),
                                                                    Attribute.of("log2",
                                                                                 attributes("message2",
                                                                                            "log-message"))));
   }

   @Test
   public void testParseWhenParseSubKeys() {
      JsonParser jsonParser =
            new JsonParser(new Parameters().withParameter("json.parser.parse-keys", "log.sub"));

      SearchResult result = newResult(attributes("log", attributes("sub", "{\"message\":\"log-message\"}")));

      SearchResult parseResult = jsonParser.parse(result);

      assertThat(parseResult.total()).isEqualTo(1);
      assertThat(parseResult.results()).hasSize(1);
      assertThat(parseResult.results().get(0)).isEqualTo(attributes("log",
                                                                    attributes("sub", attributes("message",
                                                                                          "log-message"))));
   }

   @Test
   public void testParseWhenUnknownParseSubKeys() {
      JsonParser jsonParser =
            new JsonParser(new Parameters().withParameter("json.parser.parse-keys", "log.unknown"));

      SearchResult result = newResult(attributes("log", attributes("sub", "{\"message\":\"log-message\"}")));

      SearchResult parseResult = jsonParser.parse(result);

      assertThat(parseResult.total()).isEqualTo(1);
      assertThat(parseResult.results()).hasSize(1);
      assertThat(parseResult.results().get(0)).isEqualTo(attributes("log",
                                                                    attributes("sub",
                                                                               "{\"message\":\"log-message\"}")));
   }

   @Test
   public void testParseWhenUnknownParseKeysForSubKeys() {
      JsonParser jsonParser =
            new JsonParser(new Parameters().withParameter("json.parser.parse-keys", "unknown.sub"));

      SearchResult result = newResult(attributes("log", attributes("sub", "{\"message\":\"log-message\"}")));

      SearchResult parseResult = jsonParser.parse(result);

      assertThat(parseResult.total()).isEqualTo(1);
      assertThat(parseResult.results()).hasSize(1);
      assertThat(parseResult.results().get(0)).isEqualTo(attributes("log",
                                                                    attributes("sub",
                                                                               "{\"message\":\"log-message\"}")));
   }

   @Test
   public void testParseWhenEscapedParseKeys() {
      JsonParser jsonParser =
            new JsonParser(new Parameters().withParameter("json.parser.parse-keys", "log\\\\\\..sub"));

      SearchResult result =
            newResult(attributes("log\\.", attributes("sub", "{\"message\":\"log-message\"}")));

      SearchResult parseResult = jsonParser.parse(result);

      assertThat(parseResult.total()).isEqualTo(1);
      assertThat(parseResult.results()).hasSize(1);
      assertThat(parseResult.results().get(0)).isEqualTo(attributes("log\\.",
                                                                    attributes("sub",
                                                                               attributes("message",
                                                                                          "log-message"))));
   }

   @Test
   public void testParseWhenParseSubKeysRecursively() {
      JsonParser jsonParser =
            new JsonParser(new Parameters().withParameter("json.parser.parse-keys", "log.sub,log"));

      SearchResult result = newResult(attributes("log", "{\"sub\":{\"message\":\"log-message\"}}"));

      SearchResult parseResult = jsonParser.parse(result);

      assertThat(parseResult.total()).isEqualTo(1);
      assertThat(parseResult.results()).hasSize(1);
      assertThat(parseResult.results().get(0)).isEqualTo(attributes("log",
                                                                    attributes("sub", attributes("message",
                                                                                          "log-message"))));
   }

   @Test
   public void testParseWhenParseKeysWithValueNode() {
      checkParsedValue("\"32\"", "32");
      checkParsedValue("32", 32);
      checkParsedValue("true", true);
      checkParsedValue("32.0", 32.0);
   }

   @Test
   public void testParseWhenParseKeysWithArrayNode() {
      checkParsedValue("[ \"32\" , 32 ]", "[\"32\",32]");
   }

   @Test
   public void testParseWhenParseKeysWithNullNode() {
      checkParsedValue("null", null);
   }

   @Test
   public void testParseWhenParseKeysWithObjectNode() {
      checkParsedValue("{ \"key\" : \"32\" }", attributes("key", "32"));
      checkParsedValue("{ \"key\" : 32 }", attributes("key", 32));
      checkParsedValue("{ \"key\" : true }", attributes("key", true));
      checkParsedValue("{ \"key\" : 32.0 }", attributes("key", 32.0));
      checkParsedValue("{ \"key\" : [ \"32\" , 32 ] }", attributes("key", "[\"32\",32]"));
      checkParsedValue("{ \"key\" : null }", attributes("key", null));
   }

   @Test
   public void testParseWhenMergeRootKeys() {
      JsonParser jsonParser = new JsonParser(new Parameters().withParameter("json.parser.merge-keys", "log"));

      SearchResult result = newResult(attributes("log", attributes("message", "log-message")));

      SearchResult parseResult = jsonParser.parse(result);

      assertThat(parseResult.total()).isEqualTo(1);
      assertThat(parseResult.results()).hasSize(1);
      assertThat(parseResult.results().get(0)).isEqualTo(attributes("message", "log-message"));
   }

   @Test
   public void testParseWhenMergeSubKeys() {
      JsonParser jsonParser =
            new JsonParser(new Parameters().withParameter("json.parser.merge-keys", "log.app"));

      SearchResult result =
            newResult(attributes("log", attributes("app", attributes("message", "log-message"))));

      SearchResult parseResult = jsonParser.parse(result);

      assertThat(parseResult.total()).isEqualTo(1);
      assertThat(parseResult.results()).hasSize(1);
      assertThat(parseResult.results().get(0)).isEqualTo(attributes("log",
                                                                    attributes("message", "log-message")));
   }

   @Test
   public void testParseWhenMergeKeysRecursively() {
      JsonParser jsonParser =
            new JsonParser(new Parameters().withParameter("json.parser.merge-keys", "log.app,log"));

      SearchResult result =
            newResult(attributes("log", attributes("app", attributes("message", "log-message"))));

      SearchResult parseResult = jsonParser.parse(result);

      assertThat(parseResult.total()).isEqualTo(1);
      assertThat(parseResult.results()).hasSize(1);
      assertThat(parseResult.results().get(0)).isEqualTo(attributes("message", "log-message"));
   }

   @Test
   public void testParseWhenMergeKeysWithValueNode() {
      JsonParser jsonParser = new JsonParser(new Parameters().withParameter("json.parser.merge-keys", "log"));

      SearchResult result = newResult(attributes("log", 32));

      SearchResult parseResult = jsonParser.parse(result);

      assertThat(parseResult.total()).isEqualTo(1);
      assertThat(parseResult.results()).hasSize(1);
      assertThat(parseResult.results().get(0)).isEqualTo(attributes("log", 32));
   }

   @Test
   public void testParseWhenMergeKeysWithExclusions() {
      JsonParser jsonParser =
            new JsonParser(new Parameters().withParameter("json.parser.merge-keys", "log!message1"));

      SearchResult result = newResult(attributes("log",
                                                 attributes(Attribute.of("message1", "log-message"),
                                                            Attribute.of("message2", "log-message"),
                                                            Attribute.of("message3", "log-message"))));

      SearchResult parseResult = jsonParser.parse(result);

      assertThat(parseResult.total()).isEqualTo(1);
      assertThat(parseResult.results()).hasSize(1);
      assertThat(parseResult.results().get(0)).isEqualTo(attributes(Attribute.of("message2", "log-message"),
                                                                    Attribute.of("message3", "log-message")));
   }

   @Test
   public void testParseWhenMergeKeysWithUnknownExclusions() {
      JsonParser jsonParser =
            new JsonParser(new Parameters().withParameter("json.parser.merge-keys", "log!unknown"));

      SearchResult result = newResult(attributes("log",
                                                 attributes(Attribute.of("message1", "log-message"),
                                                            Attribute.of("message2", "log-message"),
                                                            Attribute.of("message3", "log-message"))));

      SearchResult parseResult = jsonParser.parse(result);

      assertThat(parseResult.total()).isEqualTo(1);
      assertThat(parseResult.results()).hasSize(1);
      assertThat(parseResult.results().get(0)).isEqualTo(attributes(Attribute.of("message1", "log-message"),
                                                                    Attribute.of("message2", "log-message"),
                                                                    Attribute.of("message3", "log-message")));
   }

   @Test
   public void testParseWhenMergeKeysWithInvalidExclusions() {
      JsonParser jsonParser =
            new JsonParser(new Parameters().withParameter("json.parser.merge-keys", "log!"));

      SearchResult result = newResult(attributes("log",
                                                 attributes(Attribute.of("message1", "log-message"),
                                                            Attribute.of("message2", "log-message"),
                                                            Attribute.of("message3", "log-message"))));

      SearchResult parseResult = jsonParser.parse(result);

      assertThat(parseResult.total()).isEqualTo(1);
      assertThat(parseResult.results()).hasSize(1);
      assertThat(parseResult.results().get(0)).isEqualTo(attributes(Attribute.of("message1", "log-message"),
                                                                    Attribute.of("message2", "log-message"),
                                                                    Attribute.of("message3", "log-message")));
   }

   @Test
   public void testParseWhenMergeKeysWithMultipleExclusions() {
      JsonParser jsonParser =
            new JsonParser(new Parameters().withParameter("json.parser.merge-keys", "log!message1!message3"));

      SearchResult result = newResult(attributes("log",
                                                 attributes(Attribute.of("message1", "log-message"),
                                                            Attribute.of("message2", "log-message"),
                                                            Attribute.of("message3", "log-message"))));

      SearchResult parseResult = jsonParser.parse(result);

      assertThat(parseResult.total()).isEqualTo(1);
      assertThat(parseResult.results()).hasSize(1);
      assertThat(parseResult.results().get(0)).isEqualTo(attributes(Attribute.of("message2", "log-message")));
   }

   @Test
   public void testParseWhenParseKeysAndMergeKeysRecursively() {
      JsonParser jsonParser = new JsonParser(new Parameters()
                                                   .withParameter("json.parser.parse-keys", "log,log.app")
                                                   .withParameter("json.parser.merge-keys", "log.app,log"));

      SearchResult result = newResult(attributes("log", "{\"app\":{\"message\":\"log-message\"}}"));

      SearchResult parseResult = jsonParser.parse(result);

      assertThat(parseResult.total()).isEqualTo(1);
      assertThat(parseResult.results()).hasSize(1);
      assertThat(parseResult.results().get(0)).isEqualTo(attributes("message", "log-message"));
   }

   private void checkParsedValue(String value, Object expectedValue) {
      JsonParser jsonParser = new JsonParser(new Parameters() {{
         put("json.parser.parse-keys", "log");
      }});

      SearchResult result = newResult(attributes("log", value));

      SearchResult parseResult = jsonParser.parse(result);

      assertThat(parseResult.total()).isEqualTo(1);
      assertThat(parseResult.results()).hasSize(1);
      assertThat(parseResult.results().get(0)).isEqualTo(attributes("log", expectedValue));
   }

   private Attributes attributes(String key, Object value) {
      return new Attributes().withAttribute(key, value);
   }

   private Attributes attributes(Attribute... attributes) {
      return new Attributes().withAttributes(attributes);
   }

   private SearchResult newResult(Attributes attributes) {
      return new SearchResult(singletonList(attributes), 1L);
   }

}