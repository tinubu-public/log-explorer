/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.jinjava.filters;

import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.hubspot.jinjava.interpret.JinjavaInterpreter;
import com.hubspot.jinjava.lib.filter.Filter;

/**
 * Pad content to left or right.
 * <p>
 * Syntax : pad(length[, position])
 * <p>
 * Position :
 * <ul>
 * <li>right (default)</li>
 * <li>left</li>
 * </ul>
 *
 * @implSpec ANSI codes are discarded in the pad length computation.
 */
public class PadFilter implements Filter {

   /** ANSI matching pattern borrowed from https://github.com/chalk/ansi-regex/blob/master/index.js */
   private static final Pattern ANSI_PATTERN = Pattern.compile(
         "[\\u001B\\u009B][\\[\\]()#;?]*(?:(?:(?:[a-zA-Z\\d]*(?:;[-a-zA-Z\\d\\/#&.:=?%@~_]*)*)?\\u0007)|(?:(?:\\d{1,4}(?:;\\d{0,4})*)?[\\dA-PR-TZcf-ntqry=><~]))");

   @Override
   public Object filter(Object var, JinjavaInterpreter interpreter, String... args) {
      if (args.length == 0) {
         throw new IllegalArgumentException(String.format(
               "'%s' filter must have at least one pad length attribute",
               getName()));
      }

      String content = (String) var;
      String ansiFreeContent = ANSI_PATTERN.matcher(content).replaceAll("");
      String padLocation = "right";
      if (args.length > 1) {
         padLocation = args[1];
      }

      int padLength = Integer.parseInt(args[0]) + (content.length() - ansiFreeContent.length());
      switch (padLocation) {
         case "left":
            return StringUtils.leftPad(content, padLength);
         case "right":
         default:
            return StringUtils.rightPad(content, padLength);
      }
   }

   @Override
   public String getName() {
      return "pad";
   }
}
