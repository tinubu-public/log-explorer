/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.jinjava.filters;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static org.fusesource.jansi.Ansi.Color.BLUE;
import static org.fusesource.jansi.Ansi.Color.CYAN;
import static org.fusesource.jansi.Ansi.Color.RED;
import static org.fusesource.jansi.Ansi.Color.YELLOW;
import static org.fusesource.jansi.Ansi.ansi;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;

import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.Ansi.Attribute;

import com.hubspot.jinjava.interpret.JinjavaInterpreter;
import com.hubspot.jinjava.lib.filter.Filter;

/**
 * Special treatment for log levels.
 * Treatments :
 * <ul>
 * <li>Uppercase</li>
 * <li>ANSI coloration</li>
 * </ul>
 */
public class LogLevelFilter implements Filter {

   private static Map<String, BiFunction<String, Ansi, Ansi>> levelAttributes = new HashMap<>();

   static {
      levelAttributes.put("DEB", (level, ansi) -> ansi.fg(CYAN).a(level).reset()); // DEBUG
      levelAttributes.put("INF", (level, ansi) -> ansi.fg(BLUE).a(level).reset()); // INFO
      levelAttributes.put("WAR", (level, ansi) -> ansi.fg(YELLOW).bold().a(level).reset()); // WARN
      levelAttributes.put("ERR", (level, ansi) -> ansi.fg(RED).bold().a(level).reset()); // ERROR
      levelAttributes.put("TRA",
                          (level, ansi) -> ansi.a(Attribute.INTENSITY_FAINT).a(level).reset()); // TRACE
      levelAttributes.put("FINE",
                          (level, ansi) -> ansi.a(Attribute.INTENSITY_FAINT).a(level).reset()); // FINE
      levelAttributes.put("FINEST",
                          (level, ansi) -> ansi.a(Attribute.INTENSITY_FAINT).a(level).reset()); // FINEST
   }

   @Override
   public Object filter(Object var, JinjavaInterpreter interpreter, String... args) {
      notNull(var, "var");

      String level = ((String) var).toUpperCase();

      Optional<String> levelKey = levelAttributes.keySet().stream().filter(level::startsWith).findFirst();

      if (levelKey.isPresent()) {
         return levelAttributes
               .getOrDefault(levelKey.get(), (l, a) -> a.a(l))
               .apply(level, ansi())
               .toString();
      } else {
         return level;
      }
   }

   @Override
   public String getName() {
      return "logLevel";
   }
}
