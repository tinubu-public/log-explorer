/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.jinjava.filters;

import static org.fusesource.jansi.Ansi.ansi;

import com.hubspot.jinjava.interpret.JinjavaInterpreter;
import com.hubspot.jinjava.lib.filter.Filter;

/**
 * Filter for ANSI codes.
 * <p>
 * Syntax : ansi(attribute1[, attribute2[, ...]])
 * <p>
 * Attributes :
 * <ul>
 * <li>foreground colors : red=fg_red, green=fg_green, yellow=fg_yellow, blue=fg_blue, magenta=fg_magenta,
 * cyan=fg_cyan, white=fg_white</li>
 * <li>background colors : bg_red, bg_green, bg_yellow, bg_blue, bg_magenta, bg_cyan, bg_white</li>
 * <li>attributes : reset, bold=intensity_bold, faint=intensity_faint, italic, underline, blink_slow,
 * blink_fast, blink_off, negative_on, negative_off, conceal_on, conceal_off, underline_double,
 * underline_off</li>
 * </ul>
 */
public class AnsiFilter implements Filter {

   @Override
   public Object filter(Object var, JinjavaInterpreter interpreter, String... args) {
      if (args.length == 0) {
         throw new IllegalArgumentException(String.format("'%s' filter must have at least one color attribute",
                                                          getName()));
      }

      String ansiFormat = "@|" + String.join(",", args) + " " + var + "|@";

      return ansi().render(ansiFormat).toString();
   }

   @Override
   public String getName() {
      return "ansi";
   }
}
