/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.raw;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import static java.time.temporal.ChronoField.HOUR_OF_DAY;
import static java.time.temporal.ChronoField.MINUTE_OF_HOUR;
import static java.time.temporal.ChronoField.NANO_OF_SECOND;
import static java.time.temporal.ChronoField.SECOND_OF_MINUTE;
import static java.util.stream.Collectors.joining;
import static org.fusesource.jansi.Ansi.ansi;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.Temporal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.Ansi.Attribute;

import com.tinubu.commons.lang.util.Pair;
import com.tinubu.logexplorer.core.config.Configuration.Parameters;
import com.tinubu.logexplorer.core.extension.parameter.ParameterDescription;
import com.tinubu.logexplorer.core.extension.parameter.completers.BooleanCompleter;
import com.tinubu.logexplorer.core.formatter.Formatter;
import com.tinubu.logexplorer.core.search.SearchResult.Attributes;

public class RawFormatter implements Formatter {

   /**
    * Date time formatter with 3 significant digits for fraction.
    */
   public static final DateTimeFormatter DATE_TIME_FORMATTER = new DateTimeFormatterBuilder()
         .parseCaseInsensitive()
         .append(ISO_LOCAL_DATE)
         .appendLiteral(' ')
         .append(new DateTimeFormatterBuilder()
                       .appendValue(HOUR_OF_DAY, 2)
                       .appendLiteral(':')
                       .appendValue(MINUTE_OF_HOUR, 2)
                       .optionalStart()
                       .appendLiteral(':')
                       .appendValue(SECOND_OF_MINUTE, 2)
                       .optionalStart()
                       .appendFraction(NANO_OF_SECOND, 3, 3, true)
                       .toFormatter())
         .toFormatter();
   /**
    * Date time formatter with offset display.
    */
   private static final DateTimeFormatter TIME_OFFSET_FORMATTER = new DateTimeFormatterBuilder()
         .parseCaseInsensitive()
         .append(DATE_TIME_FORMATTER)
         .parseLenient()
         .appendOffsetId()
         .parseStrict()
         .toFormatter();
   /**
    * Date time formatter with offset and zone id display.
    */
   private static final DateTimeFormatter TIME_ZONEID_FORMATTER = new DateTimeFormatterBuilder()
         .parseCaseInsensitive()
         .append(DATE_TIME_FORMATTER)
         .parseLenient()
         .appendOffsetId()
         .optionalStart()
         .appendLiteral('[')
         .parseCaseSensitive()
         .appendZoneRegionId()
         .appendLiteral(']')
         .parseStrict()
         .toFormatter();

   private static final String JOIN_SEPARATOR = ", ";
   private static final String KEY_VALUE_SEPARATOR = "=";

   /** Display zone id parameter definition. */
   private static final Pair<String, Boolean> DISPLAY_ZONE_ID_PARAMETER =
         Pair.of("raw.formatter.display-zone-id", false);
   /** Pretty parameter definition. */
   private static final Pair<String, Boolean> PRETTY_PARAMETER = Pair.of("raw.formatter.pretty", true);
   /** Single line message parameter definition. */
   private static final Pair<String, Boolean> SINGLE_LINE_MESSAGE_PARAMETER =
         Pair.of("raw.formatter.single-line-message", false);

   private static final Pattern SINGLE_LINE_MESSAGE_PATTERN = Pattern.compile("\\s*(?:\\r|\\r\\n|\\n)\\s*");

   private final ZoneId timezone;
   private final boolean pretty;
   private final DateTimeFormatter dateTimeFormatter;
   private final boolean singleLineMessage;

   public RawFormatter(Parameters parameters, ZoneId timezone) {
      notNull(parameters, "parameters");
      this.timezone = notNull(timezone, "timezone");
      this.pretty = parameters.booleanValue(PRETTY_PARAMETER.getKey()).orElse(PRETTY_PARAMETER.getValue());
      this.singleLineMessage = parameters
            .booleanValue(SINGLE_LINE_MESSAGE_PARAMETER.getKey())
            .orElse(SINGLE_LINE_MESSAGE_PARAMETER.getValue());
      this.dateTimeFormatter = parameters
                                     .booleanValue(DISPLAY_ZONE_ID_PARAMETER.getKey())
                                     .orElse(DISPLAY_ZONE_ID_PARAMETER.getValue())
                               ? TIME_ZONEID_FORMATTER
                               : TIME_OFFSET_FORMATTER;
   }

   class RawFormatterParameters {
      private final Parameters parameters;

      public RawFormatterParameters(Parameters parameters) {
         this.parameters = parameters;
      }

   }

   @Override
   public void format(StringBuilder stringBuilder, Attributes attributes) {
      stringBuilder.append(formatMap(attributes, 0));
   }

   @SuppressWarnings("unchecked")
   private String formatMap(Map<String, Object> attributes, int level) {
      return attributes.entrySet()
            .stream()
            .sorted(this::attributeComparator)
            .map(this::applyTimezone)
            .map(e -> ansiAttribute(e, level))
            .collect(joining(JOIN_SEPARATOR + (pretty ? "\n" : ""),
                             ansiLevel(ansi(), level).bold().a("{" + (pretty ? "\n" : "")).reset().toString(),
                             ansiLevel(ansi(), level)
                                   .bold()
                                   .a((pretty ? "\n" + "  ".repeat(level) : "") + "}")
                                   .reset()
                                   .toString()));
   }

   @SuppressWarnings("unchecked")
   private String ansiAttribute(Entry<String, Object> e, int level) {
      Ansi ansi = ansi().a(pretty ? "  ".repeat(level + 1) : "");
      ansiLevel(ansi, level);
      ansi = ansi.a(e.getKey()).reset().a(KEY_VALUE_SEPARATOR);
      if (e.getValue() instanceof Map) {
         ansi = ansi.a(formatMap((Map<String, Object>) e.getValue(), level + 1));
      } else {
         ansi = ansi.fgBlue().a(ansiAttributeValue(e.getValue())).reset();
      }
      return ansi.toString();
   }

   private Object ansiAttributeValue(Object value) {
      if (value instanceof String && singleLineMessage) {
         return SINGLE_LINE_MESSAGE_PATTERN
               .matcher(((String) value).strip())
               .replaceAll(ansi().a(" ").fgBrightRed().a("|").reset().a(" ").toString());
      } else {
         return value;
      }
   }

   /**
    * Manages ansi colors for inner attributes levels.
    */
   private Ansi ansiLevel(Ansi ansi, int level) {
      if (level == 0) return ansi.fgYellow().bold();
      else if (level == 1) return ansi.fgGreen();
      else return ansi.fgCyan().a(Attribute.INTENSITY_FAINT);
   }

   /**
    * Apply configuration timezone to {@link ZonedDateTime} attributes.
    */
   private Entry<String, Object> applyTimezone(Entry<String, Object> attribute) {
      if (attribute.getValue() instanceof ZonedDateTime) {
         attribute.setValue(this.dateTimeFormatter.format(((ZonedDateTime) attribute.getValue()).withZoneSameInstant(
               timezone)));
      }
      return attribute;
   }

   /**
    * Use natural attribute key order, except if attribute value is a {@link Temporal}, then give it
    * higher priority.
    */
   private int attributeComparator(Entry<String, Object> a1, Entry<String, Object> a2) {
      if (a1.getValue() instanceof Temporal) {
         return -100;
      } else if (a2.getValue() instanceof Temporal) {
         return 100;
      } else {
         return a1.getKey().compareTo(a2.getKey());
      }
   }

   @Override
   public List<ParameterDescription<?>> parameters() {
      return Arrays.asList(new ParameterDescription<>(DISPLAY_ZONE_ID_PARAMETER.getKey(),
                                                      Boolean.class,
                                                      DISPLAY_ZONE_ID_PARAMETER.getValue(),
                                                      "display zone id in addition to offset in timestamps").completer(
                                 new BooleanCompleter()),
                           new ParameterDescription<>(PRETTY_PARAMETER.getKey(),
                                                      Boolean.class,
                                                      PRETTY_PARAMETER.getValue(),
                                                      "pretty print mode").completer(new BooleanCompleter()),
                           new ParameterDescription<>(SINGLE_LINE_MESSAGE_PARAMETER.getKey(),
                                                      Boolean.class,
                                                      SINGLE_LINE_MESSAGE_PARAMETER.getValue(),
                                                      "display multi-line messages on a single line").completer(
                                 new BooleanCompleter()));
   }

   @Override
   public String displayString() {
      return RawFormatterService.FORMATTER_NAME;
   }

   @Override
   public String toString() {
      return new ToStringBuilder(this)
            .append("timezone", timezone)
            .append("pretty", pretty)
            .append("singleLineMessage", singleLineMessage)
            .append("dateTimeFormatter", dateTimeFormatter)
            .toString();
   }
}
