/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;
import static java.util.Optional.ofNullable;

import java.io.File;
import java.nio.file.Paths;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Stream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.logexplorer.cli.converters.FlexibleDateTimeConverter;
import com.tinubu.logexplorer.core.backend.Backend;
import com.tinubu.logexplorer.core.backend.BackendConfiguration;
import com.tinubu.logexplorer.core.backend.BackendFactory;
import com.tinubu.logexplorer.core.config.AbstractConfiguration;
import com.tinubu.logexplorer.core.config.Configuration;
import com.tinubu.logexplorer.core.config.Configuration.Parameters;
import com.tinubu.logexplorer.core.config.Configuration.ProgressMeterFlag;
import com.tinubu.logexplorer.core.config.Configuration.QueryGroup;
import com.tinubu.logexplorer.core.config.ConfigurationBuilder;
import com.tinubu.logexplorer.core.config.DirectConfiguration;
import com.tinubu.logexplorer.core.config.YamlConfiguration;
import com.tinubu.logexplorer.core.export.Exporter;
import com.tinubu.logexplorer.core.export.ExporterFactory;
import com.tinubu.logexplorer.core.extension.parameter.ParameterDescription;
import com.tinubu.logexplorer.core.formatter.Formatter;
import com.tinubu.logexplorer.core.formatter.FormatterChain;
import com.tinubu.logexplorer.core.formatter.FormatterFactory;
import com.tinubu.logexplorer.core.parser.Parser;
import com.tinubu.logexplorer.core.parser.ParserFactory;
import com.tinubu.logexplorer.core.progress.ProgressMeter;
import com.tinubu.logexplorer.core.progress.ProgressMeterFactory;
import com.tinubu.logexplorer.core.search.ParallelSearch;
import com.tinubu.logexplorer.core.search.Search;
import com.tinubu.logexplorer.core.search.SequentialSearch;

public class Context implements AutoCloseable {

   private static final Logger logger = LoggerFactory.getLogger(Context.class);

   private static final File USER_CONFIGURATION =
         Paths.get(System.getProperty("user.home"), ".log-explorer.yml").toFile();
   private static final boolean PARALLEL_SEARCH =
         Boolean.parseBoolean(System.getProperty("log-explorer.parallel-search", "true"));

   private String fromDate;
   private String toDate;
   private File outputFile;
   private boolean appendOutputFile;
   private LinkedHashSet<String> profiles;
   private boolean debugMode;
   private boolean followMode;

   private final boolean consoleMode;
   private final File configFile;

   private Configuration configuration;
   private Search search;
   private Parser parser;
   private Formatter formatter;
   private Backend backend;
   private Exporter exporter;

   private Context(String fromDate,
                   String toDate,
                   File outputFile,
                   boolean appendOutputFile,
                   LinkedHashSet<String> profiles, boolean debugMode,
                   boolean followMode,
                   boolean consoleMode,
                   File configFile,
                   Configuration configuration,
                   Search search,
                   Parser parser,
                   Formatter formatter,
                   Backend backend,
                   Exporter exporter) {
      this.fromDate = fromDate;
      this.toDate = toDate;
      this.outputFile = outputFile;
      this.appendOutputFile = appendOutputFile;
      this.profiles = notNull(profiles, "profiles");
      this.debugMode = debugMode;
      this.followMode = followMode;
      this.consoleMode = consoleMode;
      this.configFile = configFile;
      this.configuration = configuration;
      this.search = search;
      this.parser = parser;
      this.formatter = formatter;
      this.backend = backend;
      this.exporter = exporter;

      configureDebugMode();
   }

   /** Create context with initial configuration. */
   public static Context create(String fromDate,
                                String toDate,
                                File outputFile,
                                boolean appendOutputFile,
                                LinkedHashSet<String> profiles, boolean debugMode,
                                boolean followMode,
                                boolean consoleMode,
                                File configFile,
                                Configuration... extraConfigurations) {
      Context context = new Context(fromDate,
                                    toDate,
                                    outputFile,
                                    appendOutputFile,
                                    profiles, debugMode,
                                    followMode,
                                    consoleMode,
                                    configFile,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null);

      context.configuration = context.buildConfiguration(null, extraConfigurations);

      return context;
   }

   /**
    * Copy constructor for sub-contexts.
    *
    * @implSpec After copy, original context's closeable resources are nullified, so that sub-context
    *       can take responsibility for closing resources, without resources to be closed several times.
    */
   protected Context(Context context) {
      this(context.fromDate,
           context.toDate,
           context.outputFile,
           context.appendOutputFile,
           context.profiles, context.debugMode,
           context.followMode,
           context.consoleMode,
           context.configFile,
           context.configuration,
           context.search,
           context.parser,
           context.formatter,
           context.backend,
           context.exporter);
      context.formatter = null;
      context.backend = null;
      context.exporter = null;
   }

   public String fromDate() {
      return fromDate;
   }

   public void fromDate(String fromDate) {
      this.fromDate = satisfies(fromDate,
                                fd -> !fromZonedDateTime(fd).isAfter(toZonedDateTime()),
                                "fromDate",
                                "must be <= toDate");
   }

   public ZonedDateTime fromZonedDateTime(String fromDate) {
      FlexibleDateTimeConverter dateRangeConverter =
            new FlexibleDateTimeConverter(configuration().timezone());
      return dateRangeConverter.convert(fromDate);
   }

   public ZonedDateTime fromZonedDateTime() {
      return fromZonedDateTime(fromDate);
   }

   public String toDate() {
      return toDate;
   }

   public void toDate(String toDate) {
      this.toDate = satisfies(toDate,
                              td -> !fromZonedDateTime().isAfter(toZonedDateTime(td)),
                              "toDate",
                              "must be >= fromDate");
   }

   public ZonedDateTime toZonedDateTime(String toDate) {
      FlexibleDateTimeConverter dateRangeConverter =
            new FlexibleDateTimeConverter(configuration().timezone());
      return dateRangeConverter.convert(toDate);
   }

   public ZonedDateTime toZonedDateTime() {
      return toZonedDateTime(toDate);
   }

   public void outputFile(File outputFile, boolean append) {
      this.outputFile = outputFile;
      this.appendOutputFile = append;
      finalizeContext(configuration());
   }

   public void timezone(ZoneId timezone) {
      this.configuration = buildConfiguration(configuration(), config -> config.withTimezone(timezone));
   }

   public LinkedHashSet<String> profiles() {
      return new LinkedHashSet<>(profiles);
   }

   /**
    * Resets profiles and rebuild configuration with current context.
    *
    * @param profiles optional list of new profiles to load
    *
    * @implSpec if an error occurs while rebuilding configuration, current profiles list is not updated.
    */
   public void resetProfiles(LinkedHashSet<String> profiles) {
      LinkedHashSet<String> saveProfiles = new LinkedHashSet<>(this.profiles);
      this.profiles = new LinkedHashSet<>(notNull(profiles, "profiles"));
      try {
         this.configuration = buildConfiguration(configuration);
      } catch (Exception e) {
         this.profiles = saveProfiles;
         throw e;
      }
   }

   /**
    * Resets configuration queries.
    */
   public void resetQueries() {
      this.configuration =
            new DirectConfiguration(configuration()).setQuery(new QueryGroup()).setNotQuery(new QueryGroup());
      finalizeContext(configuration());
   }

   /**
    * Resets configuration parameters.
    */
   public void resetParameters() {
      this.configuration = new DirectConfiguration(configuration()).setParameters(new Parameters());
      finalizeContext(configuration());
   }

   /**
    * Resets all.
    *
    * @implSpec Maintains debug state after reset.
    */
   public void resetAll() {
      this.profiles = new LinkedHashSet<>();
      this.configuration = buildConfiguration(new DirectConfiguration());
   }

   /**
    * Loads supplementary profile into context.
    * If profile is already loaded, it is reordered to the end of the list.
    *
    * @implSpec if an error occurs while rebuilding configuration, current profiles list is not updated.
    */
   public void addProfile(String profile) {
      LinkedHashSet<String> saveProfiles = new LinkedHashSet<>(this.profiles);
      this.profiles.remove(notNull(profile, "profile"));
      this.profiles.add(profile);
      try {
         this.configuration = buildConfiguration(configuration);
      } catch (Exception e) {
         this.profiles = saveProfiles;
         throw e;
      }
   }

   /**
    * Deletes profile from context.
    *
    * @throws IllegalStateException if profile is not currently loaded
    * @implSpec We want to remove the profile from the configuration, but keep some interactively
    *       updated settings, so we create a customized pre-configuration that resets queries.
    * @implSpec if an error occurs while rebuilding configuration, current profiles list is not updated.
    */
   public void removeProfile(String profile) {
      LinkedHashSet<String> saveProfiles = new LinkedHashSet<>(this.profiles);
      if (!this.profiles.remove(notNull(profile, "profile"))) {
         throw new IllegalStateException(String.format("Profile '%s' is not loaded", profile));
      }
      try {
         Configuration preConfiguration = new DirectConfiguration(configuration)
               .setQuery(new QueryGroup())
               .setNotQuery(new QueryGroup());
         this.configuration = buildConfiguration(preConfiguration);
      } catch (Exception e) {
         this.profiles = saveProfiles;
         throw e;
      }
   }

   public boolean followMode() {
      return followMode;
   }

   public void followMode(boolean followMode) {
      this.followMode = followMode;
   }

   /**
    * Overrides current configuration parameters.
    *
    * @param parameters parameters to set
    */
   public void parameters(Parameters parameters) {
      ((AbstractConfiguration) configuration()).setParameters(parameters);
      finalizeContext(configuration());
   }

   /**
    * Returns only currently loaded extensions parameter descriptions.
    *
    * @return only currently loaded extensions parameter descriptions.
    */
   public List<ParameterDescription<?>> availableParameters() {
      List<ParameterDescription<?>> parameters = new ArrayList<>();
      parameters.addAll(backend.parameters());
      parameters.addAll(parser.parameters());
      parameters.addAll(formatter.parameters());
      return parameters;
   }

   public void updateParser(String name) {
      notBlank(name, "name");

      this.configuration = buildConfiguration(configuration(), config -> config.withParser(name));
   }

   private void updateParser(Configuration configuration) {
      this.parser = createParser(configuration);
   }

   private Parser createParser(Configuration configuration) {
      return ParserFactory.instance(configuration.parser(), configuration.parameters());
   }

   public void updateFormatter(String name, String template) {
      notBlank(name, "name");

      this.configuration = buildConfiguration(configuration(),
                                              config -> config.withFormatter(new Configuration.Formatter(name,
                                                                                                         template)));
   }

   private void updateFormatter(Configuration configuration) {
      this.formatter = createFormatter(configuration);
   }

   private Formatter createFormatter(Configuration configuration) {
      String configurationFormatter = configuration.formatter().name();

      FormatterChain formatterChain = FormatterChain
            .empty()
            .withFormatter(FormatterFactory.instance(configurationFormatter,
                                                     configuration.parameters(),
                                                     configuration.timezone(),
                                                     configuration.formatter().template()));

      if (!configurationFormatter.equals("raw") && !configurationFormatter.equals("noop")) {
         formatterChain = formatterChain.withFormatter(FormatterFactory.instance("raw",
                                                                                 configuration.parameters(),
                                                                                 configuration.timezone(),
                                                                                 null));
      }

      if (!configurationFormatter.equals("noop")) {
         formatterChain = formatterChain.withFormatter(FormatterFactory.instance("noop",
                                                                                 configuration.parameters(),
                                                                                 configuration.timezone(),
                                                                                 null));
      }

      return formatterChain;
   }

   public void updateBackend(String name) {
      notBlank(name, "name");

      this.configuration = buildConfiguration(configuration(), config -> config.withBackend(name));
   }

   private void updateBackend(Configuration configuration) {
      closeBackendClient();
      this.backend = createBackend(configuration);
   }

   /** Instantiates backend. */
   private Backend createBackend(Configuration configuration) {
      BackendConfiguration backendConfiguration = new BackendConfiguration()
            .backendUris(configuration.backendUris())
            .connectTimeout(configuration.connectTimeout())
            .socketTimeout(configuration.socketTimeout())
            .authenticationToken(configuration.authenticationToken())
            .authenticationUser(configuration.authenticationUser())
            .authenticationPassword(configuration.authenticationPassword());

      return BackendFactory.instance(configuration.backend(),
                                     configuration.parameters(),
                                     backendConfiguration, debugMode());
   }

   private void closeBackendClient() {
      if (this.backend != null) {
         this.backend.close();
      }
   }

   private void updateExporter(Configuration configuration) {
      closeExporter();
      this.exporter = createExporter(configuration);
   }

   private Exporter createExporter(Configuration configuration) {
      return outputFile == null
             ? ExporterFactory.instance(System.out, formatter)
             : ExporterFactory.instance(outputFile, appendOutputFile, formatter);
   }

   private void closeExporter() {
      if (this.exporter != null) {
         this.exporter.close();
      }
   }

   private void closeSearch() {
      if (this.search != null) {
         this.search.close();
      }
   }

   private ProgressMeter createProgressMeter(Configuration configuration) {
      ProgressMeterFlag displayProgressMeter =
            consoleMode ? ProgressMeterFlag.NEVER : configuration.progress();

      if (displayProgressMeter == ProgressMeterFlag.AUTO) {
         displayProgressMeter = outputFile != null ? ProgressMeterFlag.ALWAYS : ProgressMeterFlag.NEVER;
      }

      return ProgressMeterFactory.instance(displayProgressMeter == ProgressMeterFlag.ALWAYS, System.err);
   }

   public Search search() {
      return search;
   }

   public boolean debugMode() {
      return debugMode;
   }

   public void debugMode(boolean debug) {
      this.debugMode = debug;
      configureDebugMode();
   }

   /**
    * Configures logging depending on debug mode.
    */
   private void configureDebugMode() {
      Configurator.setLevel("com.tinubu.logexplorer",
                            Boolean.TRUE.equals(this.debugMode) ? Level.DEBUG : Level.INFO);
   }

   public void numberLines(Long numberLines) {
      this.configuration = buildConfiguration(configuration(), config -> config.withNumberLines(numberLines));
   }

   public Set<String> availableProfiles() {
      File configurationFile = configurationFile();
      return new HashSet<>(YamlConfiguration.availableProfiles(configurationFile.toURI(), true, true));
   }

   public Configuration configuration() {
      return configuration;
   }

   /**
    * Resolves the final configuration applying all configuration layers.
    *
    * @param extraConfigurations extra configurations to apply to base configuration
    *
    * @return final configuration
    */
   private Configuration buildConfiguration(Configuration preConfiguration,
                                            Configuration... extraConfigurations) {
      File configurationFile = configurationFile();
      Configuration configuration = new ConfigurationBuilder()
            .chain(nullable(preConfiguration))
            .chain(new YamlConfiguration(configurationFile.toURI(), profiles, profiles.isEmpty()))
            .chains(Stream.of(extraConfigurations))
            .build();

      return finalizeContext(configuration);
   }

   public File configurationFile() {
      return ofNullable(configFile).orElse(USER_CONFIGURATION);
   }

   /**
    * Overloads the specified profile over an already loaded configuration. Only the top layers are
    * applied.
    *
    * @return overloaded configuration
    */
   private Configuration buildConfiguration(Configuration configuration, String profile) {
      File configurationFile = configurationFile();
      Configuration overloadedConfiguration = new ConfigurationBuilder()
            .chain(configuration)
            .chain(new YamlConfiguration(configurationFile.toURI(),
                                         new LinkedHashSet<>(List.of(profile)),
                                         false))
            .build();
      return finalizeContext(overloadedConfiguration);
   }

   /**
    * Overrides an already loaded configuration. Values will be overridden, not merged.
    *
    * @return overridden configuration
    */
   private Configuration buildConfiguration(Configuration configuration,
                                            Function<DirectConfiguration, Configuration> overrider) {
      Configuration overriddenConfiguration =
            new ConfigurationBuilder().chain(overrider.apply(new DirectConfiguration(configuration))).build();
      return finalizeContext(overriddenConfiguration);
   }

   /**
    * Finalizes the context after configuration changes.
    */
   protected Configuration finalizeContext(Configuration configuration) {
      logger.debug("Resolved configuration = {}", configuration);

      updateParser(configuration);
      updateFormatter(configuration);
      updateBackend(configuration);
      updateExporter(configuration);

      ProgressMeter progressMeter = createProgressMeter(configuration);

      logger.debug("Use parallel search : {}", PARALLEL_SEARCH);

      this.search = PARALLEL_SEARCH
                    ? new ParallelSearch(backend, progressMeter, parser, exporter)
                    : new SequentialSearch(backend, progressMeter, parser, exporter);

      return configuration;
   }

   public Formatter formatter() {
      return this.formatter;
   }

   public Parser parser() {
      return parser;
   }

   @Override
   public void close() {
      closeSearch();
      closeBackendClient();
      closeExporter();
   }

}
