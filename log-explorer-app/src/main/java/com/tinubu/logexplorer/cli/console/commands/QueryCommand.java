/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.cli.console.commands;

import static java.util.stream.Collectors.joining;
import static org.fusesource.jansi.Ansi.ansi;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.jline.builtins.Completers.OptDesc;
import org.jline.builtins.Completers.OptionCompleter;
import org.jline.reader.Completer;
import org.jline.reader.impl.completer.ArgumentCompleter;
import org.jline.reader.impl.completer.NullCompleter;
import org.jline.reader.impl.completer.StringsCompleter;

import com.tinubu.logexplorer.cli.console.ConsoleCommand;
import com.tinubu.logexplorer.cli.console.ConsoleContext;
import com.tinubu.logexplorer.core.search.QueryDateRange;
import com.tinubu.logexplorer.core.search.query.QueryBuilder;
import com.tinubu.logexplorer.core.search.query.QuerySpecification;

public class QueryCommand extends ConsoleCommand {

   @Override
   public String commandName() {
      return "query";
   }

   @Override
   public CommandHelpDescription helpDescription() {
      return new CommandHelpDescription("query backend with optional follow mode",
                                        "[-F]",
                                        "[<query>]").withVerboseDescription(verboseDescription());
   }

   private String verboseDescription() {
      // @formatter:off
      return new VerboseDescriptionBuilder()
            .appendnl("Query backend using the following syntax :")
            .appendnl()
            .appendnl("[<selector>]/<query>[/[<options>]]", ansi().fgMagenta())
            .appendnl()
            .appendnl("Query can use '/' characters if they are escaped '\\/'. Hence, '\\' characters must be escaped too with '\\\\'.")
            .appendnl()
            .appendnl("The following query types are supported (selector) :")
            .appendnl()
            .appendnl("Simple query :", ansi().fgCyan())
            .appendnl()
            .append("s/case sensitive text", ansi().fgMagenta()).appendnl(" : Case sensitive search. 'simple' prefix is also supported")
            .append("/case sensitive text", ansi().fgMagenta()).appendnl(" : Case sensitive search. Simple query is assumed when no selector is specified")
            .append("/case insensitive text/i", ansi().fgMagenta()).appendnl(" : Case insensitive search")
            .appendnl()
            .appendnl("Simple queries can be translated to native queries, depending on backend, otherwise, it will be applied on result, after parser plugins.")
            .appendnl()
            .appendnl("Uniform query :", ansi().fgCyan())
            .appendnl()
            .append("u/attribute = \"case sensitive text\"", ansi().fgMagenta()).appendnl(" : Case sensitive search on 'attribute' attribute. 'uniform' prefix is also supported")
            .append("u/attribute = \"case insensitive text\"/i", ansi().fgMagenta()).appendnl(" : Case insensitive search on 'attribute' attribute")
            .appendnl()
            .appendnl("Attribute can be complex, or not specified at all :")
            .append("u/attribute.sub-attribute = \"text\"", ansi().fgMagenta()).appendnl(" : Search on all 'attribute.sub-attribute' attribute")
            .append("u/= \"text\"", ansi().fgMagenta()).appendnl(" : Search on all available attributes")
            .appendnl("Operands can be of different types :")
            .append("u/attribute = \"string\"", ansi().fgMagenta()).appendnl(" : String attribute")
            .append("u/attribute = /string/", ansi().fgMagenta()).appendnl(" : Regexp attribute")
            .append("u/attribute = /string/i", ansi().fgMagenta()).appendnl(" : Regexp attribute with regexp options")
            .append("u/attribute = 2.3", ansi().fgMagenta()).appendnl(" : Double attribute")
            .append("u/attribute = 23", ansi().fgMagenta()).appendnl(" : Long attribute")
            .append("u/attribute = true", ansi().fgMagenta()).appendnl(" : Boolean attribute")
            .appendnl("Existing/non-existing attributes are matched using :")
            .append("u/attribute = null", ansi().fgMagenta()).appendnl(" : Non-existing attribute")
            .append("u/attribute != null", ansi().fgMagenta()).appendnl(" : Existing attribute")
            .appendnl("Supported operators are :")
            .append("u/attribute = \"string\"", ansi().fgMagenta()).appendnl(" : Strict equality operator")
            .append("u/attribute != \"string\"", ansi().fgMagenta()).appendnl(" : Strict inequality operator")
            .append("u/attribute ~ \"string\"", ansi().fgMagenta()).appendnl(" : Partial equality operator (string must be contained in attribute value)")
            .append("u/attribute !~ \"string\"", ansi().fgMagenta()).appendnl(" : Partial inequality operator (string must not be contained in attribute value)")
            .append("u/attribute < 23", ansi().fgMagenta()).appendnl(" : Numeric comparator (less than)")
            .append("u/attribute <= 23.0", ansi().fgMagenta()).appendnl(" : Numeric comparator (less than or equal to)")
            .append("u/attribute > 23", ansi().fgMagenta()).appendnl(" : Numeric comparator (greater than)")
            .append("u/attribute >= 23.0", ansi().fgMagenta()).appendnl(" : Numeric comparator (greater than or equal to)")
            .appendnl("You can escape attribute and text :")
            .appendnl("u/attribute\\.dotted\\.value = \"escaped \\\"text\\\"\"", ansi().fgMagenta())
            .appendnl()
            .appendnl("Uniform queries can be translated to native queries depending on backend, otherwise, it will be applied on result, after parser plugins.")
            .appendnl()
            .appendnl("Native query :", ansi().fgCyan())
            .appendnl()
            .append("n/native query", ansi().fgMagenta()).appendnl(" : Native query. 'native' prefix is also supported")
            .appendnl()
            .append("Native query syntax and options depends on backend.")
            .appendnl()
            .toString();
      // @formatter:on
   }

   @Override
   public void execute(ConsoleContext context, List<String> options) {
      Stream<String> queryOptions = options.stream().skip(1);
      boolean singleQueryFollowMode = false;
      if (options.size() > 1 && "-F".equals(options.get(1))) {
         singleQueryFollowMode = true;
         queryOptions = queryOptions.skip(1);
      }
      QuerySpecification querySpecification = QuerySpecification
            .empty()
            .withQuerySet(context.configuration().query().flatten())
            .withNotQuerySet(context.configuration().notQuery().flatten());
      String interactiveQuery = queryOptions.collect(joining(" ")).trim();
      if (!interactiveQuery.isEmpty()) {
         querySpecification = querySpecification.addQuery(QueryBuilder.parseQuery(interactiveQuery));
      }
      context
            .search()
            .search(querySpecification,
                    new QueryDateRange(context.fromZonedDateTime(), context.toZonedDateTime()),
                    context.configuration().numberLines(),
                    context.followMode() || singleQueryFollowMode);
   }

   @Override
   public Completer completer(ConsoleContext context) {
      return new ArgumentCompleter(new CommandCompleter(),
                                   new OptionCompleter(Arrays.asList(new StringsCompleter("-F"),
                                                                     NullCompleter.INSTANCE),
                                                       (List<OptDesc>) null,
                                                       1),
                                   NullCompleter.INSTANCE);
   }

}
