/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.cli.console.commands;

import static com.tinubu.logexplorer.core.config.YamlConfiguration.includeResource;
import static org.fusesource.jansi.Ansi.ansi;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.List;

import com.tinubu.logexplorer.cli.console.ConsoleCommand;
import com.tinubu.logexplorer.cli.console.ConsoleContext;
import com.tinubu.logexplorer.core.config.YamlConfiguration;
import com.tinubu.logexplorer.core.config.YamlConfiguration.ConfigurationModel;
import com.tinubu.logexplorer.core.lang.TypeConverter;

public class DumpConfigCommand extends ConsoleCommand {

   @Override
   public String commandName() {
      return "dumpConfiguration";
   }

   @Override
   public CommandHelpDescription helpDescription() {
      return new CommandHelpDescription("dump external configuration file");
   }

   @Override
   public int minimumParameters() {
      return 0;
   }

   @Override
   public void execute(ConsoleContext context, List<String> options) {
      try {
         File configurationFile = context.configurationFile().getCanonicalFile();

         if (configurationFile.exists()) {
            context.println(ansi()
                                  .fgBlue().a("Dump configuration model from '")
                                  .fgCyan()
                                  .a(configurationFile.getCanonicalPath())
                                  .fgBlue()
                                  .a("' configuration file")
                                  .reset());
            context.println();
            printConfiguration(context, configurationFile.toURI());
         } else {
            context.println(ansi()
                                  .fgBlue()
                                  .a("Can't dump non-existing '")
                                  .fgCyan()
                                  .a(configurationFile.getCanonicalPath())
                                  .fgBlue()
                                  .a("' configuration file")
                                  .reset());
         }

      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   private static void printConfiguration(ConsoleContext context, URI resource) {
      ConfigurationModel model = YamlConfiguration.configurationModel(resource, false, false);

      for (URI include : TypeConverter.uriListValue(model.getInclude())) {
         printConfiguration(context, includeResource(resource, include));
         context.println(ansi().fgRed().a("---").reset());
      }
      context.println(ansi().fgRed().a("# ").a(resource).reset());
      context.printObject(model);
      context.println();
   }

}
