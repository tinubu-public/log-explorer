/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.cli.console.commands;

import static java.util.stream.Collectors.joining;

import java.io.File;
import java.nio.file.Paths;
import java.util.List;

import org.jline.builtins.Completers.FilesCompleter;
import org.jline.reader.Completer;
import org.jline.reader.impl.completer.ArgumentCompleter;
import org.jline.reader.impl.completer.NullCompleter;

import com.tinubu.logexplorer.cli.console.ConsoleCommand;
import com.tinubu.logexplorer.cli.console.ConsoleContext;

public class OutputCommand extends ConsoleCommand {

   @Override
   public String commandName() {
      return "output";
   }

   @Override
   public CommandHelpDescription helpDescription() {
      return new CommandHelpDescription("output to specified file (append mode with +) or disable file output",
                                        "[[+]<file>]");
   }

   @Override
   public void execute(ConsoleContext context, List<String> options) {
      String file = options.size() > 1 ? options.stream().skip(1).collect(joining(" ")) : null;
      boolean append = file != null && file.startsWith("+");
      if (append) {
         file = file.substring(1);
      }

      if (file == null) {
         context.outputFile(null, false);
      } else {
         context.outputFile(new File(file), append);
      }
   }

   @Override
   public Completer completer(ConsoleContext context) {
      return new ArgumentCompleter(new CommandCompleter(),
                                   new FilesCompleter(Paths.get("")),
                                   NullCompleter.INSTANCE);
   }
}
