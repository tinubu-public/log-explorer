/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.cli.console.commands;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.lang.Math.max;
import static java.util.Collections.unmodifiableList;
import static java.util.stream.Collectors.toList;
import static org.fusesource.jansi.Ansi.ansi;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import org.fusesource.jansi.Ansi.Color;
import org.jline.reader.Candidate;
import org.jline.reader.Completer;
import org.jline.reader.LineReader;
import org.jline.reader.ParsedLine;
import org.jline.reader.impl.completer.AggregateCompleter;
import org.jline.reader.impl.completer.ArgumentCompleter;
import org.jline.reader.impl.completer.NullCompleter;

import com.tinubu.logexplorer.cli.console.ConsoleCommand;
import com.tinubu.logexplorer.cli.console.ConsoleContext;

public class AliasCommand extends ConsoleCommand {

   private static final Color HELP_COMMAND_COLOR = HelpCommand.HELP_COMMAND_COLOR;
   private static final Color HELP_DESCRIPTION_COLOR = HelpCommand.HELP_DESCRIPTION_COLOR;
   private static final Color HELP_OPTION_COLOR = HelpCommand.HELP_OPTION_COLOR;

   @Override
   public String commandName() {
      return "alias";
   }

   @Override
   public CommandHelpDescription helpDescription() {
      return new CommandHelpDescription("create an alias for target command with options",
                                        "<alias>",
                                        "<command>",
                                        "[<options>...]");
   }

   @Override
   public int minimumParameters() {
      return 2;
   }

   @Override
   public void execute(ConsoleContext context, List<String> options) {
      context.addCommand(context.createAliasTargetCommand(options.get(1),
                                                          options.stream().skip(2).collect(toList())));
   }

   @Override
   public Completer completer(ConsoleContext context) {
      return new ArgumentCompleter(new CommandCompleter(),
                                   new AnyMatchCompleter(),
                                   new AggregateCompleter(context
                                                                .commands()
                                                                .stream()
                                                                .filter(command -> !(command instanceof AliasCommand)
                                                                                   && !(command instanceof AliasTargetCommand))
                                                                .map(command -> new AliasCommandCompleter(
                                                                      command,
                                                                      context))
                                                                .collect(toList())));
   }

   /** Special completer wrap aliased commands completers. */
   public static class AliasCommandCompleter extends ArgumentCompleter {
      private final boolean strictCommand = true;

      public AliasCommandCompleter(ConsoleCommand aliasTargetCommand, ConsoleContext context) {
         super(((ArgumentCompleter) aliasTargetCommand.completer(context)).getCompleters());
      }

      @Override
      public void complete(LineReader reader, ParsedLine line, List<Candidate> candidates) {
         Objects.requireNonNull(line);
         Objects.requireNonNull(candidates);

         if (line.wordIndex() < 0) {
            return;
         }

         if (line.wordIndex() > 0) {
            ParsedLine finalLine = line;
            line = new ParsedLine() {
               @Override
               public String word() {
                  return finalLine.word();
               }

               @Override
               public int wordCursor() {
                  return finalLine.wordCursor();
               }

               @Override
               public int wordIndex() {
                  return finalLine.wordIndex() - 2;
               }

               @Override
               public List<String> words() {
                  return finalLine.words().stream().skip(2).collect(toList());
               }

               @Override
               public String line() {
                  return finalLine.line();
               }

               @Override
               public int cursor() {
                  return finalLine.cursor() - finalLine.words().get(0).length() - finalLine
                        .words()
                        .get(1)
                        .length() - 2;
               }
            };

            List<Completer> completers = getCompleters();
            Completer completer;

            // if we are beyond the end of the completers, just use the last one
            if (line.wordIndex() >= completers.size()) {
               completer = completers.get(completers.size() - 1);
            } else {
               completer = completers.get(line.wordIndex());
            }

            // ensure that all the previous completers are successful before allowing this completer to pass (only if strict).
            for (int i = strictCommand ? 0 : 1; isStrict() && (i < line.wordIndex()); i++) {
               int idx = i >= completers.size() ? (completers.size() - 1) : i;
               if (idx == 0 && !strictCommand) {
                  continue;
               }
               Completer sub = completers.get(idx);
               List<? extends CharSequence> args = line.words();
               String arg = (args == null || i >= args.size()) ? "" : args.get(i).toString();

               List<Candidate> subCandidates = new LinkedList<>();
               sub.complete(reader, new ArgumentLine(arg, arg.length()), subCandidates);

               boolean found = false;
               for (Candidate cand : subCandidates) {
                  if (cand.value().equals(arg)) {
                     found = true;
                     break;
                  }
               }
               if (!found) {
                  return;
               }
            }

            completer.complete(reader, line, candidates);
         } else {
            candidates.add(new Candidate(line.word()));
         }
      }
   }

   public static class AliasTargetCommand extends ConsoleCommand {

      private final String aliasCommand;
      private final List<String> aliasOptions;
      private final ConsoleCommand targetCommand;

      public AliasTargetCommand(String aliasCommand,
                                List<String> aliasOptions,
                                ConsoleCommand targetCommand) {
         this.aliasCommand = notNull(aliasCommand, "aliasCommand");
         this.aliasOptions = unmodifiableList(aliasOptions);
         this.targetCommand = notNull(targetCommand, "targetCommand");
      }

      @Override
      public String commandName() {
         return aliasCommand;
      }

      @Override
      public int minimumParameters() {
         return targetCommand.minimumParameters() - aliasOptions.size();
      }

      @Override
      public void execute(ConsoleContext context, List<String> options) {
         List<String> targetOptions = new ArrayList<>();
         targetOptions.add(targetCommand.commandName());
         targetOptions.addAll(aliasOptions);
         targetOptions.addAll(options.stream().skip(1).collect(toList()));

         targetCommand.execute(context, targetOptions);
      }

      @Override
      public CommandHelpDescription helpDescription() {
         List<String> extraOptions = targetCommand
               .helpDescription()
               .options()
               .stream()
               .skip(aliasOptions.size())
               .map(this::normalizeOption)
               .collect(toList());

         return new CommandHelpDescription(ansi()
                                                 .a("alias for '")
                                                 .fg(HELP_COMMAND_COLOR)
                                                 .a(targetCommand.commandName())
                                                 .a(!aliasOptions.isEmpty()
                                                    ? (" " + String.join(" ",
                                                                         aliasOptions))
                                                    : "")
                                                 .a(!extraOptions.isEmpty()
                                                    ? (ansi()
                                                             .a(" ")
                                                             .fg(HELP_OPTION_COLOR)
                                                             .a(String.join(" ",
                                                                            extraOptions)))
                                                    : "")
                                                 .fg(HELP_DESCRIPTION_COLOR)
                                                 .a("'")
                                                 .toString(), extraOptions);
      }

      /**
       * When borrowing a partial set of options from source command help description, some square brackets
       * can require some cleanups. e.g.: {@code [ '[[value1]', '[value2]]]', '[[!]value3]' ] -> [ '[value1]',
       * '[value2]', '[[!]value3]' ]}.
       */
      String normalizeOption(String option) {
         long openingCount = option.chars().filter(c -> c == '[').count();
         long closingCount = option.chars().filter(c -> c == ']').count();

         String normalizedOption = option;
         for (int i = 0; i < max(0, openingCount - closingCount); i++) {
            normalizedOption = normalizedOption.replaceFirst("^\\[\\[", "[");
         }
         for (int i = 0; i < max(0, closingCount - openingCount); i++) {
            normalizedOption = normalizedOption.replaceFirst("]]$", "]");
         }

         return normalizedOption;
      }

      @Override
      public Completer completer(ConsoleContext context) {
         Completer targetCompleter = targetCommand.completer(context);

         if (targetCompleter instanceof ArgumentCompleter) {
            List<Completer> completers = new ArrayList<>();
            completers.add(new CommandCompleter());
            completers.addAll(((ArgumentCompleter) targetCompleter)
                                    .getCompleters()
                                    .stream()
                                    .skip(1 + aliasOptions.size())
                                    .collect(toList()));

            return new ArgumentCompleter(completers);
         } else {
            return new ArgumentCompleter(new CommandCompleter(), NullCompleter.INSTANCE);
         }
      }

      public String aliasCommand() {
         return aliasCommand;
      }

      public List<String> aliasOptions() {
         return aliasOptions;
      }

      public ConsoleCommand targetCommand() {
         return targetCommand;
      }
   }
}
